#version 150

in vec2 Vertex;
in vec2 Texcoord;

noperspective out vec2 v2TexCoord;

void main() {
	v2TexCoord = Vertex.xy * 0.5 + vec2(0.5);
	gl_Position = vec4(Vertex.xy, 0.0f, 1.0f);
}