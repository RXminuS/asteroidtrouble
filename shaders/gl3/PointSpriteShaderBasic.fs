#version 150
in float flogz;
uniform float fcoef;

out vec4 fColor;

void main(){
    fColor = vec4(0.5f, 0.5f, 1.0f, 1.0f);
    gl_FragDepth = log2(flogz) * fcoef * 0.5f;
}