#version 150

in vec3 Vertex;
//in vec3 Tangent;
//in vec3 Binormal;
in vec3 Normal;
in vec3 Texcoord;

uniform mat4 World;					/* transform to world space */
uniform mat4 WorldViewProjection;	/* transform to screen space */
uniform mat4 WorldInverseTranspose;

uniform vec3 ViewPosition;

//out mat3 TBN;
out vec3 fN;
out vec3 fV;
out vec2 texCoord;

void main() {
	//TBN = mat3(Tangent, Binormal, Normal);

	vec3 worldPos = (World*vec4(Vertex,1)).xyz;
	fV = ViewPosition - worldPos;
	fN = (WorldInverseTranspose*vec4(Normal,0)).xyz;
	texCoord = Texcoord.xy;

	gl_Position = WorldViewProjection*vec4(Vertex,1);
}