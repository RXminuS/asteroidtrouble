#version 150

in vec3 Vertex;
in vec3 Normal;
in vec3 Texcoord;

out float flogz;

uniform mat4 WorldViewProjection;

uniform float fcoef;

void main() {
	gl_Position = WorldViewProjection*vec4(Vertex, 1.0f);
	gl_Position.z = log2(max(1e-6, 1.0 + gl_Position.w)) * fcoef - 1.0;
	flogz = 1.0 + gl_Position.w;
}