#version 150
uniform sampler2D displacementTex;
uniform sampler2D colorRampTex;
uniform vec3 channelFactor;
uniform float displacement;
uniform vec3 range = vec3(0, 0.5f, 0);
uniform float clipRange = 0.8f;
uniform float fcoef;

in float flogz;
in vec2 v2TexCoord;

out vec4 fColor;

void main() {
    vec3 dcolor = texture(displacementTex, v2TexCoord).rgb;
    float d = (dcolor.r*channelFactor.r + dcolor.g*channelFactor.g + dcolor.b*channelFactor.b ) * (range.y - range.x) + range.x;
    if( clipRange < d)
        discard;
    fColor.rgb = pow(texture(colorRampTex, vec2(d, 0.5)).rgb, vec3(6.0))*150.0;
	fColor.a = 1.0;
    gl_FragDepth = log2(flogz) * fcoef * 0.5f;
}
