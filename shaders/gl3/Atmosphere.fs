#version 150

uniform float fG;
uniform float fG2;
uniform vec3 v3LightDir;
uniform float fcoef; 

in vec3 v3Direction;
in vec3 v3Mie;
in vec3 v3Rayleigh;
in float flogz;

out vec4 fColor;

// Calculates the Mie phase function
float getMiePhase(float fCos, float fCos2, float g, float g2)
{
	return 1.5 * ((1.0 - g2) / (2.0 + g2)) * (1.0 + fCos2) / pow(1.0 + g2 - 2.0*g*fCos, 1.5);
}

// Calculates the Rayleigh phase function
float getRayleighPhase(float fCos2)
{
	//return 1.0;
	return 0.75 + 0.75*fCos2;
}

void main() {
	fColor = vec4(0.0,0.0,0.0,0.0);
	vec3 v3LightDirNorm = normalize(v3LightDir);
	float fCos = dot(v3LightDirNorm, v3Direction) / length(v3Direction);
	float fCos2 = fCos*fCos;
	fColor.rgb = getRayleighPhase(fCos2) * v3Rayleigh + getMiePhase(fCos, fCos2, fG, fG2) * v3Mie;
	fColor.a = 1.0 - exp(-5*fColor.b);
    gl_FragDepth = log2(flogz) * fcoef * 0.5f;
}

