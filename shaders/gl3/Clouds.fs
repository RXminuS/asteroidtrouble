#version 150

uniform sampler2D DiffuseTex;
uniform vec3 LightDir;

//in mat3 TBN;
in vec3 fN;
in vec3 fV;
in vec2 texCoord;

out vec4 fColor;

void main() {
	vec3 L = normalize(LightDir);
	vec3 V = normalize(fV);
	vec3 N = normalize(fN);

	vec4 diffSample = texture(DiffuseTex, texCoord);
	//Calculate Bump Map Normal
	//bumpSample = bumpSample * 2 - vec3(1.0f); //Map from [0,1] to [-1,1];
	//vec4 objectNormal = vec4(TBN*bumpSample,0.0);

	vec3 R = normalize(reflect(-L,N));
	float ldn = dot(L,N);
	vec3 diffuse = diffSample.xyz*max(ldn, 0.0);
	
	fColor.xyz = diffuse;
	fColor.w = diffSample.w;
}
