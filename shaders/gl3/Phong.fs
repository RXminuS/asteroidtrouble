#version 150

uniform vec3 ka;
uniform vec3 kd;
uniform vec3 ks;
uniform float shininess;

in vec3 fN;
in vec3 fL;
in vec3 fV;

out vec4 fColor;

void main() {
	vec3 R = normalize(reflect(-fL,fN));
	vec3 diffuse = kd*max(dot(fL, fN), 0.0);
	vec3 specular = ks*pow(max(dot(fV, R),0.0), shininess);
	fColor.xyz = ka + diffuse + specular;
	fColor.w = 1.0;
}
