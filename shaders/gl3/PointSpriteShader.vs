#version 150

in vec3 Pos;
in vec3 Vel;
in float Age;

uniform float fcoef;
uniform float uBirthSize = 30.0;
uniform float uDeathSize = 0.0;
uniform float uMaxAge = 1.0;

out float flogz;
out float fLife;

uniform mat4 ViewProjection;	/* transform to screen space */

void main() {
	fLife = Age/uMaxAge;
	gl_Position = ViewProjection*vec4(Pos, 1.0f);
	gl_Position.z = log2(max(1e-6, 1.0 + gl_Position.w)) * fcoef - 1.0;
    gl_PointSize = mix(uBirthSize, uDeathSize, fLife)/ gl_Position.w;
	flogz = 1.0 + gl_Position.w;
}



