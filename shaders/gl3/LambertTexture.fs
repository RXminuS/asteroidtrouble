#version 150

uniform vec3 ka;
uniform vec3 lightColor = vec3(3.0);

uniform sampler2D DiffuseTex;
uniform float fcoef;

in vec3 fL;
in vec3 fV;
in vec3 fN;
in vec2 texCoord;
in float flogz;

out vec4 fColor;

void main() {
    vec3 ka_ = vec3(0.05);
    vec3 ks_ = vec3(1.0);
    float shininess_ = 150.0;
	vec3 L = normalize(fL);
	vec3 V = normalize(fV);
	vec3 N = normalize(fN);

	vec3 diffSample = texture(DiffuseTex, texCoord).rgb;

	vec3 R = normalize(reflect(-L, N));
	float ldn = dot(L,N);
	vec3 diffuse = diffSample*(ka_ + max(ldn, 0.0))*lightColor;

	fColor.xyz = diffuse;
	fColor.w = 1.0;
    gl_FragDepth = log2(flogz) * fcoef * 0.5f;
}
