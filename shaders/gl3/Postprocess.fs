#version 150
/*******************************************************************************
	Copyright (C) 2013 John Chapman

	This software is distributed freely under the terms of the MIT License.
	See "license.txt" or "http://copyfree.org/licenses/mit/license.txt".
*******************************************************************************/

uniform sampler2D uInputTex;
//uniform sampler2D uDepthTex;
uniform sampler2D uAdaptedLuminanceTex;

uniform float uExposure = 1.0;

noperspective in vec2 v2TexCoord;

out vec4 fColor;

/*----------------------------------------------------------------------------*/
float log10(in float x) {
	return log2(x) / log2(10);
}

/*----------------------------------------------------------------------------*/
vec2 powv2(in vec2 b, in float e) {
	return vec2(pow(b.x, e), pow(b.y, e));
}
/*----------------------------------------------------------------------------*/
vec3 powv3(in vec3 b, in float e) {
	return vec3(pow(b.x, e), pow(b.y, e), pow(b.z, e));
}
/*----------------------------------------------------------------------------*/
vec4 powv4(in vec4 b, in float e) {
	return vec4(pow(b.x, e), pow(b.y, e), pow(b.z, e), pow(b.w, e));
}

/*----------------------------------------------------------------------------*/
float luminance(in vec3 rgb) {
	const vec3 kLum = vec3(0.2126, 0.7152, 0.0722);
	return max(dot(rgb, kLum), 0.0001); // prevent zero result
}

/*----------------------------------------------------------------------------*/
/*	Grzegorz Krawczyk's dynamic key function. */
float autokey(in float lum) {
	return 1.03 - 2.0 / (2.0 + log10(lum + 1.0));
}

/*----------------------------------------------------------------------------*/
/*	Jim Hejl's ALU filmic tonemapping. This incorporates the output gamma 
	correction. */
vec4 hejl(in vec4 color) {
	vec4 x = max(vec4(0.0), color - vec4(0.004));
	return (x * (6.2 * x + 0.5)) / (x * (6.2 * x + 1.7) + 0.06);
}

void main()
{
    //vec4 lensMod = texture(uLensDirtTex, vTexcoord);
	//vec2 lensStarTexcoord = (uLensStarMatrix * vec3(vTexcoord, 1.0)).xy;
	//lensMod += texture(uLensStarTex, lensStarTexcoord);

	float vignette = 1.0 - length(vec2(0.5) - v2TexCoord) / length(vec2(0.5));
	vignette = pow(vignette, 0.8);
	
	//vec4 result = motionBlur();
	
	//result += texture(uLensFlareTex, vTexcoord) * lensMod;
	vec4 result = texture(uInputTex, v2TexCoord);	
	float adaptedLum = texture(uAdaptedLuminanceTex, vec2(0.5)).r;
	float exposure = uExposure * autokey(adaptedLum) / adaptedLum;
	exposure *= vignette;
	result *= exposure;		
	
	fColor = hejl(result);
}

