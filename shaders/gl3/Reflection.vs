#version 150

/* Per pixel reflection shader */

/* variables per vertex
*/
in vec3 Vertex;
in vec3 Normal;

/* variables per primitive
*/
uniform mat4 World;					/* transform to world space */
uniform mat4 WorldViewProjection;	/* transform to screen space */
uniform mat4 WorldInverseTranspose;

uniform vec3 ViewPosition;

out vec3 fV; //View Vector
out vec3 fN; //Normal Vector

void main() {
	vec3 worldPos = (World*vec4(Vertex,1)).xyz;
	fN = (WorldInverseTranspose * vec4(Normal.xyz, 1.0)).xyz;
	fV = ViewPosition - worldPos.xyz;

	gl_Position = WorldViewProjection*vec4(Vertex,1);
}