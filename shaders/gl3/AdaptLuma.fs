#version 150

uniform sampler2D uLuminanceTex;
uniform sampler2D uPrevLuminanceTex;
uniform float uAdaptionRate = 0.001;

noperspective in vec2 v2TexCoord;

out float fColor;

void main()
{
    float avgLum = texture(uLuminanceTex, vec2(0.5)).r;
    float prevLum = texture(uPrevLuminanceTex, vec2(0.5)).r;
    float adaptedLum = prevLum + (avgLum - prevLum) * (1.0 - exp2(-uAdaptionRate));
	fColor = adaptedLum;
}