#version 150

in vec2 Vertex;
in vec2 Texcoord;

noperspective out vec2 v2TexCoord;


void main() {
	v2TexCoord = Texcoord.xy;
	gl_Position = vec4(Vertex.x, Vertex.y, 0.0f, 1.0f);
}