#version 150
/*******************************************************************************
	Copyright (C) 2013 John Chapman
*******************************************************************************/
uniform sampler2D uInputTex;

uniform vec4 uScale = vec4(1.0);
uniform vec4 uBias = vec4(0.0);

noperspective in vec2 v2TexCoord;
out vec4 fColor;

/*----------------------------------------------------------------------------*/
void main() {
	fColor = max(vec4(0.0), texture(uInputTex, v2TexCoord) + uBias) * uScale;
}
