#version 150

in vec3 Pos;

uniform float fcoef;
uniform float spriteSize = 300.0f;
out float flogz;
uniform mat4 WorldViewProjection;	

void main() {
	gl_Position = WorldViewProjection*vec4(Pos, 1.0f);
	gl_Position.z = log2(max(1e-6, 1.0 + gl_Position.w)) * fcoef - 1.0;
    gl_PointSize = spriteSize / gl_Position.w;
	flogz = 1.0 + gl_Position.w;
}



