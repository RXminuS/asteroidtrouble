uniform samplerCube CubeMapTex;

in vec3 fV;
in vec3 fN;

out vec4 fColor;

void main() {
	vec3 V = normalize(fV);
	vec3 N = normalize(fN);
	vec3 R = normalize(reflect(-V, N));
	fColor = texture(CubeMapTex, R);
	fColor.w = 1.0;
}
