#version 150

uniform sampler2D uInputTex;
noperspective in vec2 v2TexCoord;

out vec3 fColor; // r = average, g = min, b = max

void main() {
    const vec2 kSampleTexcoord[4] = vec2[4](
		vec2(-1.0, -1.0),
		vec2(1.0, -1.0),
		vec2(1.0, 1.0),
		vec2(-1.0, 1.0)
	);
	
	vec2 texelSize = 1.0 / vec2(textureSize(uInputTex, 0));
	
	vec3 result = vec3(texture(uInputTex, v2TexCoord + kSampleTexcoord[0] * texelSize).r);
	for (int i = 1; i < 4; ++i) {
		vec3 s = texture(uInputTex, v2TexCoord + kSampleTexcoord[i] * texelSize).rgb;
		result.r += s.r;
		
		result.g = min(result.g, s.g);
		result.b = max(result.b, s.b);
	}
	result.r /= 4.0;
	
	fColor = result;
}
