#version 150
in float fLife;
in float flogz;

uniform float fcoef;
uniform float uBirthOpacity = 1.0;
uniform float uDeathOpacity = 0.0;
uniform vec3  uBirthColor = vec3(1.0, 1.0, 1.0);
uniform vec3  uDeathColor = vec3(1.0, 0.0, 1.0);

uniform sampler2D uInputTex;

out vec4 fColor;

void main(){
    fColor.rgb   = mix(uBirthColor, uDeathColor, fLife);
    fColor.a     = mix(uBirthOpacity, uDeathOpacity, fLife);
    fColor       *= texture(uInputTex, vec2(gl_PointCoord));//, 0.0, 1.0);//texture(uInputTex, gl_PointCoord.st);
    gl_FragDepth = log2(flogz) * fcoef * 0.5f;
}