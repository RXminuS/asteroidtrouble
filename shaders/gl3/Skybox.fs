#version 150
uniform samplerCube SkyboxTex;

in vec3 fV;
in vec3 fN;

out vec4 fColor;

void main() {
	vec3 N = normalize(fN);
	fColor = texture(SkyboxTex, N);
    fColor = fColor;
	fColor.w = 1.0;
}
