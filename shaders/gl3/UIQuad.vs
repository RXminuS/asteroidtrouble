#version 150

in vec2 Vertex;
in vec2 Texcoord;

out vec2 v2TexCoord;

uniform mat4 WorldViewProjection;
float aspect = 1280.0f/800.0f;
void main() {
	v2TexCoord = Texcoord.xy;
	gl_Position = WorldViewProjection*vec4(Vertex.x, Vertex.y, 0.0f, 1.0f);
}