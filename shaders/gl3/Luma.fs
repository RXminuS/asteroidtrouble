#version 150

uniform sampler2D uInputTex;
noperspective in vec2 v2TexCoord;
out vec4 fColor;

float luminance(in vec3 rgb) {
	const vec3 kLum = vec3(0.2126, 0.7152, 0.0722);
	return max(dot(rgb, kLum), 0.0001); // prevent zero result
}

void main()
{
	vec3 color = texture(uInputTex, v2TexCoord).rgb;
	fColor = vec4(luminance(color),0.0f, 0.0f, 1.0);
}