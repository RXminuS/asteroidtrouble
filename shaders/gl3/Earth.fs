#version 150

uniform sampler2D DiffuseTex;
uniform sampler2D NormalTex;
uniform sampler2D SpecularTex;

uniform vec3 lightColor = vec3(3.0);
uniform vec3 v3SpecularColor;
uniform vec3 v3LightDir;
uniform float fSpecularPower;
uniform float fcoef;
uniform mat4 WorldInverseTranspose;

in mat3 TBN;
in vec3 fV; //View Vector
in vec2 v2TexCoord;
in vec3 v3PrimaryColor;
in vec3 v3SecondaryColor;
in vec3 v3ObjectNormal;
in float flogz;

out vec4 fColor;

void main()
{
	vec3 L = normalize(v3LightDir);
	vec3 V = normalize(fV);

	vec3 diffSample = texture(DiffuseTex, v2TexCoord).rgb;
	vec3 bumpSample = texture(NormalTex, v2TexCoord).rgb;
	float specularSample = texture(SpecularTex, v2TexCoord).x;

	bumpSample = bumpSample * 2 - vec3(1.0);
	vec4 objectNormal = vec4(TBN*bumpSample,0.0);
	vec3 N = normalize(WorldInverseTranspose*objectNormal).xyz;

	float ldn = dot(L,N);
		
	vec3 v3DiffuseColor = v3PrimaryColor + max(0.0,ldn)*(v3SecondaryColor*diffSample);
	v3DiffuseColor *= lightColor;
    vec3 R = normalize(reflect(-L,N));
	vec3 v3SpecularColor = vec3(0.0);
	if(ldn > 0.0)
	{
		v3SpecularColor = pow(max(dot(V, R),0.0), 100.0)*lightColor;
	}
	
	fColor.rgb = v3DiffuseColor + v3SpecularColor*specularSample;
	fColor.a = 1.0;
    gl_FragDepth = log2(flogz) * fcoef * 0.5f;
}