#version 150

uniform float fcoef;
in float flogz;

out vec4 fColor;

void main() {
    fColor = vec4(192.0,150.0,120.0, 1.0)*3.0;
    fColor.w = 1.0;
    gl_FragDepth = log2(flogz) * fcoef * 0.5f;
}
