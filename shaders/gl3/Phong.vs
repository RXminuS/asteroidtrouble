#version 150

/* Per pixel Phong shader */

/* variables per vertex
*/
in vec3 Vertex;
in vec3 Normal;

/* variables per primitive
*/
uniform mat4 World;					/* transform to world space */
uniform mat4 WorldViewProjection;	/* transform to screen space */
uniform mat4 WorldInverseTranspose;	/* transform to world space (normals) */

uniform vec3 ViewPosition;
uniform vec3 LightPosition;

out vec3 fN; //Normal Vector
out vec3 fV; //View Vector
out vec3 fL; //Light Vector

void main() {

	vec3 worldPos = (World*vec4(Vertex,1)).xyz;
	fN = normalize((WorldInverseTranspose*vec4(Normal,0)).xyz);
	fV = normalize(ViewPosition - worldPos);
	fL = normalize(LightPosition - worldPos);

	gl_Position = WorldViewProjection*vec4(Vertex,1);
}