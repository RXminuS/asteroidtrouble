#version 150

in vec3 Vertex;
in vec3 Tangent;
in vec3 Binormal;
in vec3 Normal;
in vec3 Texcoord;

out mat3 TBN;
out vec3 fV; //View Vector
out vec2 v2TexCoord;
out vec3 v3PrimaryColor;
out vec3 v3SecondaryColor;
out vec3 v3ObjectNormal;
out float flogz;

uniform mat4 World;
uniform mat4 WorldInverse;
uniform mat4 WorldViewProjection;

uniform vec3	ViewPosition;
uniform vec3	v3LightDir;
uniform vec3	v3InvWavelength;
uniform float	fCameraHeight;
uniform float	fCameraHeight2;
uniform float	fOuterRadius;
uniform float	fOuterRadius2;
uniform float	fInnerRadius;
uniform float	fInnerRadius2;
uniform float	fKrESun;
uniform float	fKmESun;
uniform float	fKr4PI;
uniform float	fKm4PI;
uniform float	fScale;
uniform float	fScaleOverScaleDepth;

uniform float fcoef;

const float fScaleDepth = 0.25;
const float fInvScaleDepth = 1.0 / fScaleDepth;

const int nSamples = 2;
const float fSamples = 2.0;

float scale(float fCos)
{
	float x = 1.0 - fCos;
	return fScaleDepth * exp(-0.00287 + x*(0.459 + x*(3.83 + x*(-6.80 + x*5.25))));
}

float getNearIntersection(vec3 v3Pos, vec3 v3Ray, float fDistance2, float fRadius2)
{
	float B = 2.0 * dot(v3Pos, v3Ray);
	float C = fDistance2 - fRadius2;
	float fDet = max(0.0, B*B - 4.0 * C);
	return 0.5 * (-B - sqrt(fDet));
}

void main()
{
	vec3 v3Pos = (World*vec4(Vertex,1.0)).xyz;
	vec3 v3CameraPos = ViewPosition;
	vec3 v3Ray = v3Pos - v3CameraPos;
	v3Pos = normalize(v3Pos.xyz);
	float fFar = length(v3Ray);
	v3Ray /= fFar;

	// Calculate the closest intersection of the ray with the outer atmosphere (which is the near point of the ray passing through the atmosphere)
	float fNear = getNearIntersection(v3CameraPos, v3Ray, fCameraHeight2, fOuterRadius2);
	
	// Calculate the ray's starting position, then calculate its scattering offset
	vec3 v3Start = v3CameraPos + v3Ray * fNear;
	fFar -= fNear;
	float fDepth = exp((fInnerRadius - fOuterRadius) * fInvScaleDepth);
	float fCameraAngle = dot(-v3Ray, v3Pos);
	float fLightAngle = dot(normalize(v3LightDir), v3Pos);
	float fCameraScale = scale(fCameraAngle);
	float fLightScale = scale(fLightAngle);
	float fCameraOffset = fDepth*fCameraScale;
	float fTemp = (fLightScale + fCameraScale);

	// Initialize the scattering loop variables
	float fSampleLength = fFar / fSamples;
	float fScaledLength = fSampleLength * fScale;
	vec3 v3SampleRay = v3Ray * fSampleLength;
	vec3 v3SamplePoint = v3Start + v3SampleRay * 0.5;

	// Now loop through the sample rays
	vec3 v3FrontColor = vec3(0.0, 0.0, 0.0);
	vec3 v3Attenuate = vec3(0.0, 0.0, 0.0);
	for(int i=0; i<nSamples; i++)
	{
		float fHeight = length(v3SamplePoint);
		float fDepth = exp(fScaleOverScaleDepth * (fInnerRadius - fHeight));
		float fScatter = fDepth*fTemp - fCameraOffset;
		v3Attenuate = exp(-fScatter * (v3InvWavelength * fKr4PI + fKm4PI));
		v3FrontColor += v3Attenuate * (fDepth * fScaledLength);
		v3SamplePoint += v3SampleRay;
		v3SecondaryColor = v3FrontColor;
	}
	
	v2TexCoord = Texcoord.xy;
	gl_Position = WorldViewProjection*vec4(Vertex,1);
	gl_Position.z = log2(max(1e-6, 1.0 + gl_Position.w)) * fcoef - 1.0;
	flogz = 1.0 + gl_Position.w;
	v3PrimaryColor = v3FrontColor * (v3InvWavelength * fKrESun + fKmESun);
	v3SecondaryColor = v3Attenuate;
	TBN = mat3(Tangent, Binormal, Normal);
	v3ObjectNormal = Normal;
	vec3 v3WorldPos = (World*vec4(Vertex,1)).xyz;
	fV = v3CameraPos - v3WorldPos;
}