#version 150

uniform sampler2D Tex;
in vec2 v2TexCoord;

out vec4 fColor;

void main()
{
	fColor = texture(Tex, v2TexCoord);
}