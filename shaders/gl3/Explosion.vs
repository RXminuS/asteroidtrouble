#version 150

in vec3 Vertex;
in vec3 Normal;
in vec3 Texcoord;

out vec2 v2TexCoord;
out float flogz;

uniform sampler2D displacementTex;
uniform mat4 WorldViewProjection;
uniform vec3 channelFactor = vec3(1.0f, 0.0f, 0.0f);
uniform float displacement = 3.0f;

uniform float fcoef;

void main() {
	vec3 dcolor = texture2D(displacementTex, Texcoord.xy).rgb;
	float d = (dcolor.r*channelFactor.r + dcolor.g*channelFactor.g + dcolor.b*channelFactor.b );
	v2TexCoord = Texcoord.xy;
	gl_Position = WorldViewProjection*vec4(Vertex + Normal * d * displacement, 1.0f);
	gl_Position.z = log2(max(1e-6, 1.0 + gl_Position.w)) * fcoef - 1.0;
	flogz = 1.0 + gl_Position.w;
}