#version 150

/* Per pixel Phong shader */

/* variables per vertex
*/
in vec3 Vertex;
in vec3 Normal;
in vec3 Texcoord;

/* variables per primitive
*/
uniform mat4 World;					/* transform to world space */
uniform mat4 WorldInverseTranspose;
uniform mat4 WorldViewProjection;	/* transform to screen space */

uniform vec3 ViewPosition;
uniform vec3 LightPosition = vec3(0.0);

uniform float fcoef;

out vec3 fV; //View Vector
out vec3 fL; //Light Vector
out vec3 fN; //Normal Vector
out vec2 texCoord;
out float flogz;

void main() {
	vec3 worldPos = (World*vec4(Vertex,1)).xyz;
	fV = ViewPosition - worldPos;
	fL = LightPosition;
	fN = (WorldInverseTranspose*vec4(Normal,0)).xyz;

	texCoord = Texcoord.xy;

	gl_Position = WorldViewProjection*vec4(Vertex,1);
	gl_Position.z = log2(max(1e-6, 1.0 + gl_Position.w)) * fcoef - 1.0;
	flogz = 1.0 + gl_Position.w;
}