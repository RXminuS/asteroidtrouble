#ifndef EARTH_H
#define EARTH_H

#include "RenderChimp.h"
#include "PlanetaryObject.h"

class Earth : public PlanetaryObject
{
public:
	virtual void init(Node* root, Light* light);
	virtual void update(f32 timeStep);

protected:
	Group *earth_spin_pivot;
	Group *earth_orbital_pivot;
	Geometry *earth;
	ShaderProgram *earth_shader;
	Texture	*earth_texture;
	float earth_spin;
	float earth_tilt;
	float earth_orbital_speed;
};

#endif