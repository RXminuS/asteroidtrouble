
#ifndef RC_INTERPOLATION_H
#define RC_INTERPOLATION_H

#include "RenderChimp.h"

/*
	Linear interpolation
		
	Author(s):
*/
vec3f evalLERP(vec3f &p0, vec3f &p1, const f32 x);
vec2f evalLERP(vec2f &p0, vec2f &p1, const f32 x);
f32 evalLERP(const f32 p0, const f32 p1, const f32 x);
/*
	Catmull-Rom spline interpolation

	Author(s):
*/
vec3f evalCatmullRom(vec3f &p0, vec3f &p1, vec3f &p2, vec3f &p3, const f32 t, const f32 x);

f32 smoothDamp(const f32 current, const f32 target, f32* currentVelocity, const f32 smoothTime, const f32 maxSpeed, const f32 dt);

#endif /* RC_INTERPOLATION_H */

