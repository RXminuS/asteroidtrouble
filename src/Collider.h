#ifndef COLLIDER_H
#define COLLIDER_H

#include "RenderChimp.h"
#include <functional>

class SphereCollider : public Group{
public:
	SphereCollider(const char* nname, std::function<void(vec3f*)> handlerFunc) :
		Group(nname), handlerFunc(handlerFunc) {};
	~SphereCollider(){};

	void handleCollision(vec3f* point);
protected:
	std::function<void(vec3f*)> handlerFunc;
};

#endif