

#include "RenderChimp.h"
#include <math.h>


/*
	Circle ring

	@param res_radius
	tessellation resolution (nbr of vertices) in the radial direction ( inner_radius < radius < outer_radius )

	@param res_theta
	tessellation resolution (nbr of vertices) in the angular direction ( 0 < theta < 2PI )

	@param inner_radius
	radius of the innermost border of the ring

	@param outer_radius
	radius of the outermost border of the ring

	@return
	pointer to the generated VertexArray object 

	CJG aug 2010 - created
*/
VertexArray* createCircleRing(const i32 &res_radius, const i32 &res_theta, const f32 &inner_radius, const f32 &outer_radius)
{
	/* vertex definition */
	struct Vertex
	{
		f32 x, y, z,			/* spatial coordinates */
			texx, texy, texz,	/* texture coordinates */
			nx, ny, nz,			/* normal vector */
			tx, ty, tz,			/* tangent vector */
			bx, by, bz;			/* binormal vector */
	};

	/* vertex array */
	Vertex	*va = new Vertex[res_radius*res_theta];

	f32 theta = 0.0f,						/* 'stepping'-variable for theta: will go 0 - 2PI */
		dtheta = 2.0f*fPI/(res_theta-1);	/* step size, depending on the resolution */

	f32	radius,													/* 'stepping'-variable for radius: will go inner_radius - outer_radius */
		dradius = (outer_radius-inner_radius)/(res_radius-1);	/* step size, depending on the resolution */

	/* generate vertices iteratively */
	for (int i = 0; i < res_theta; i++)
	{
		f32	cos_theta = cosf(theta),
			sin_theta = sinf(theta);
		radius = inner_radius;

		for (int j = 0; j < res_radius; j++)
		{
			i32 vertex_index = res_radius*i + j;

			/* vertex */
			va[vertex_index].x = radius * cos_theta;
			va[vertex_index].y = radius * sin_theta;
			va[vertex_index].z = 0.0f;
			
			/* texture coordinates */
			va[vertex_index].texx = (f32)j/(res_radius-1);
			va[vertex_index].texy = (f32)i/(res_theta-1);
			va[vertex_index].texz = 0.0f;

			/* tangent */
			vec3f t = vec3f(cos_theta,
							sin_theta,
							0);
			t.normalize();

			/* binormal */
			vec3f b = vec3f(-sin_theta,
							cos_theta,
							0);
			b.normalize();
			
			/* normal */
			vec3f n = t.cross( b );
			
			va[vertex_index].nx = n.x;
			va[vertex_index].ny = n.y;
			va[vertex_index].nz = n.z;

			va[vertex_index].tx = t.x;
			va[vertex_index].ty = t.y;
			va[vertex_index].tz = t.z;

			va[vertex_index].bx = b.x;
			va[vertex_index].by = b.y;
			va[vertex_index].bz = b.z;
			
			radius += dradius;
		}

		theta += dtheta;
	}

	/* triangle def */
	struct Triangle
	{
		i32 a, b, c;		/* vertex indices */
	};

	/* create index array */
	Triangle *ia = new Triangle[ 2*(res_theta-1)*(res_radius-1) ];

	/* generate indices iteratively */
	for (int i = 0; i < res_theta-1; i++ )
	{
		for (int j = 0; j < res_radius-1; j++ )
		{
			int tri_index = 2 * ((res_radius-1)*i + j);

			ia[tri_index].a = res_radius*i+j;
			ia[tri_index].b = res_radius*i+j + 1;
			ia[tri_index].c = res_radius*i+j + 1+res_radius;

			ia[tri_index+1].a = res_radius*i+j;
			ia[tri_index+1].b = res_radius*i+j + 1+res_radius;
			ia[tri_index+1].c = res_radius*i+j + res_radius;	
		}
	}

	/* initialize the scene graph vertex array and set attributes (to be passed to shader) */
	VertexArray *cring_va = SceneGraph::createVertexArray(0, va, 15*sizeof(f32), res_radius*res_theta, TRIANGLES, USAGE_STATIC);
	cring_va->setAttribute("Vertex", 0, 3, ATTRIB_FLOAT32);
	cring_va->setAttribute("Texcoord", 3 * sizeof(f32), 3, ATTRIB_FLOAT32);
	cring_va->setAttribute("Normal", 6 * sizeof(f32), 3, ATTRIB_FLOAT32);
	cring_va->setAttribute("Tangent", 9 * sizeof(f32), 3, ATTRIB_FLOAT32);
	cring_va->setAttribute("Binormal", 12 * sizeof(f32), 3, ATTRIB_FLOAT32);

	/* set the index array */
	cring_va->setIndexArray(ia, sizeof(i32), 2*3*(res_radius-1)*(res_theta-1) );

	/* va and ia are copied within createVertexArray so we can safely release their memory */
	delete va;
    delete ia;

	return cring_va;
}


/*
	Sphere sphere

	@param res_theta
	tessellation resolution (nbr of vertices) in the theta direction ( 0 < theta < 2PI )

	@param res_phi
	tessellation resolution (nbr of vertices) in the phi direction ( 0 < phi < PI )

	@param radius
	the radius of the sphere

	@return
	pointer to the generated VertexArray object 

	Rik Nauta aug 2014 - created
*/
VertexArray* createSphere(const i32 &res_theta, const i32 &res_phi, const f32 &radius)
{
	/* vertex definition */
	struct Vertex
	{
			f32 x, y, z,			/* spatial coordinates */
			texx, texy, texz,	/* texture coordinates */
			nx, ny, nz,		/* normal vector */
			tx, ty, tz,			/* tangent vector */
			bx, by, bz;			/* binormal vector */
	};
	
	/*Vertex Array*/
	i32 vertex_count = (res_phi - 2)*res_theta+res_phi;
	Vertex *va = new Vertex[vertex_count];

	f32 theta = 0.0f,
		dtheta = 2.0f*fPI/(res_theta);

	f32 phi, /* 'stepping'-variable for phi: will go dphi -- PI-dphi*/
		dphi = fPI/(res_phi-1);

	for(int i = 0; i < res_theta + 1; i++)
	{
		f32 sin_theta = sinf(theta),
			cos_theta = cosf(theta);
		phi = dphi;

		for( int j = 0; j < res_phi - 2; j++)
		{
			i32 vertex_index = (res_phi - 2)*i + j + 1; //add top vertex
			f32 sin_phi = sinf(phi),
				cos_phi = cosf(phi);

			/*vertex*/
			va[vertex_index].x = radius * sin_theta * sin_phi;
			va[vertex_index].y = -radius * cos_phi;
			va[vertex_index].z = radius * cos_theta * sin_phi;

			/* texture coordinates */
			va[vertex_index].texx = (f32)i/(res_theta);
			va[vertex_index].texy = (f32)(j+1)/(res_phi-1);
			va[vertex_index].texz = 0.0f;

			/* tangent */
			vec3f t = vec3f(radius*cos_theta*sin_phi,
							0.0f,
							-radius*sin_theta*sin_phi);
			t.normalize();

			/* binormal */
			vec3f b = vec3f(radius*sin_theta*cos_phi,
							radius*sin_phi,
							radius*cos_theta*cos_phi);
			b.normalize();
			
			/* normal */
			vec3f n = t.cross( b );
			
			va[vertex_index].nx = n.x;
			va[vertex_index].ny = n.y;
			va[vertex_index].nz = n.z;

			va[vertex_index].tx = t.x;
			va[vertex_index].ty = t.y;
			va[vertex_index].tz = t.z;

			va[vertex_index].bx = b.x;
			va[vertex_index].by = b.y;
			va[vertex_index].bz = b.z;

			phi += dphi;
		}

		theta += dtheta;
	}
	/*Top and Bottom Vertex*/
	va[0].x = 0;
	va[0].y = - radius;
	va[0].z = 0;

	va[0].texx = 0.5f;
	va[0].texy = 0.0f;
	va[0].texz = 0.0f;

	va[0].nx = 0;
	va[0].ny = -1;
	va[0].nz = 0;

	va[0].tx = -1;
	va[0].ty = 0;
	va[0].tz = 0;

	va[0].bx = 0;
	va[0].by = 0;
	va[0].bz = -1;

	va[vertex_count-1].x = 0;
	va[vertex_count-1].y = radius;
	va[vertex_count-1].z = 0;

	va[vertex_count-1].texx = 0.5f;
	va[vertex_count-1].texy = 1.0f;
	va[vertex_count-1].texz = 0.0f;

	va[vertex_count-1].nx = 0;
	va[vertex_count-1].ny = 1;
	va[vertex_count-1].nz = 0;

	va[vertex_count-1].tx = 1;
	va[vertex_count-1].ty = 0;
	va[vertex_count-1].tz = 0;

	va[vertex_count-1].bx = 0;
	va[vertex_count-1].by = 0;
	va[vertex_count-1].bz = -1;

	/* triangle definition */
	struct Triangle
	{
		i32 a,b,c;
	};
	
	//Top and bottom give 2* res_theta triangles
	i32 triangle_count = 2*res_theta + 2*(res_theta)*(res_phi-3);
	Triangle *ia = new Triangle[triangle_count];

	for(int i = 0; i < res_theta; i++)
	{
		for(int j = 0; j < res_phi-3; j++)
		{
			int tri_index = 2 * ((res_phi-3)*i + j) + res_theta;

			ia[tri_index].a = (res_phi-2)*i+j + 1;
			ia[tri_index].b = (res_phi-2)*i+j + res_phi - 1;
			ia[tri_index].c = (res_phi-2)*i+j + 2;

			ia[tri_index+1].a = (res_phi-2)*i+j + 2;
			ia[tri_index+1].b = (res_phi-2)*i+j + res_phi - 1;
			ia[tri_index+1].c = (res_phi-2)*i+j + res_phi;
		}
	}
	/* Bottom and top triangles */
	for(int i = 0; i < res_theta; i++)
	{
		ia[i].a = 0;
		ia[i].b = (res_phi-2)*(i+1)+1;
		ia[i].c = (res_phi-2)*i+1;

		ia[triangle_count-i-1].a = vertex_count-1;
		ia[triangle_count-i-1].b = (res_phi-2)*(i+1);
		ia[triangle_count-i-1].c = (res_phi-2)*(i+2);
	}

	/* initialize the scene graph vertex array and set attributes (to be passed to shader) */
	VertexArray *sphere_va = SceneGraph::createVertexArray(0, va, 15*sizeof(f32), vertex_count, TRIANGLES, USAGE_STATIC);
	sphere_va->setAttribute("Vertex", 0, 3, ATTRIB_FLOAT32);
	sphere_va->setAttribute("Texcoord", 3 * sizeof(f32), 3, ATTRIB_FLOAT32);
	sphere_va->setAttribute("Normal", 6 * sizeof(f32), 3, ATTRIB_FLOAT32);
	sphere_va->setAttribute("Tangent", 9 * sizeof(f32), 3, ATTRIB_FLOAT32);
	sphere_va->setAttribute("Binormal", 12 * sizeof(f32), 3, ATTRIB_FLOAT32);

	/* set the index array */
	sphere_va->setIndexArray(ia, sizeof(i32),3*triangle_count);

	/* va and ia are copied within createVertexArray so we can safely release their memory */
	delete va;
    delete ia;

	return sphere_va;
}


/*
	Torus torus

	@param res_theta
	tessellation resolution (nbr of vertices) in the theta direction ( 0 < theta < 2PI )

	@param res_phi
	tessellation resolution (nbr of vertices) in the phi direction ( 0 < phi < 2PI )

	@param rA
	the radius of the doughnut hole inside the torus

	@param rB
	the radius of the torus itself

	@return
	pointer to the generated VertexArray object 

	Rik Nauta aug 2014 - created
*/
VertexArray* createTorus(const i32 &res_theta, const i32 &res_phi, const f32 &rA, const f32 &rB)
{
	/* vertex definition */
	struct Vertex
	{
			f32 x, y, z,			/* spatial coordinates */
			//texx, texy, texz,	/* texture coordinates */
			nx, ny, nz,		/* normal vector */
			tx, ty, tz,			/* tangent vector */
			bx, by, bz;			/* binormal vector */
	};

	/*Vertex Array*/
	i32 vertex_count = (res_theta+1)*(res_phi+1);
	Vertex *va = new Vertex[vertex_count];

	f32 theta = 0.0f,
		dtheta = 2*fPI/res_theta;

	f32 phi = 0.0f,
		dphi = 2*fPI/res_phi;

	/* generate vertices iteratively */
	for (int i = 0; i < res_theta+1; i++)
	{
		f32 sin_theta = sinf(theta),
			cos_theta = cosf(theta);
		phi = 0.0f;

		for (int j = 0; j < res_phi+1; j++)
		{
			i32 vertex_index = (res_phi+1)*i+j;

			f32 sin_phi = sinf(phi),
				cos_phi = cosf(phi);

			/*vertex*/
			va[vertex_index].x = (rA + rB * cos_theta) * cos_phi;
			va[vertex_index].y = (rA + rB * cos_theta) * sin_phi;
			va[vertex_index].z = -rB * sin_theta;

			/* tangent */
			vec3f t = vec3f(-rB*sin_theta*cos_phi,
							-rB*sin_theta*sin_phi,
							-rB*cos_theta);
			t.normalize();

			/* binormal */
			vec3f b = vec3f(-(rA+rB*cos_theta)*sin_phi,
							(rA + rB*cos_theta)*cos_phi,
							0);
			b.normalize();
			
			/* normal */
			vec3f n = t.cross( b );
			
			va[vertex_index].nx = n.x;
			va[vertex_index].ny = n.y;
			va[vertex_index].nz = n.z;

			va[vertex_index].tx = t.x;
			va[vertex_index].ty = t.y;
			va[vertex_index].tz = t.z;

			va[vertex_index].bx = b.x;
			va[vertex_index].by = b.y;
			va[vertex_index].bz = b.z;

			phi += dphi;
		}

		theta += dtheta;
	}

	/* triangle definition */
	struct Triangle
	{
		i32 a,b,c;
	};
	
	//Top and bottom give 2* res_theta triangles
	i32 triangle_count = 2*res_phi*res_theta;
	Triangle *ia = new Triangle[triangle_count];

	for(int i = 0; i < res_theta; i++)
	{
		for(int j = 0; j < res_phi; j++)
		{
			int tri_index = 2 * ((res_phi)*i + j);
			
			//current point = (res_phi+1)*i+j
			//point above = current_point + res_phi+1
			ia[tri_index].a = (res_phi+1)*i+j;
			ia[tri_index].b = (res_phi+1)*i+j + res_phi + 1;
			ia[tri_index].c = (res_phi+1)*i+j + 1;

			ia[tri_index+1].a = (res_phi+1)*i+j + 1;
			ia[tri_index+1].b = (res_phi+1)*i+j + res_phi + 1;
			ia[tri_index+1].c = (res_phi+1)*i+j + res_phi + 2;
		}
	}
	
	/* initialize the scene graph vertex array and set attributes (to be passed to shader) */
	VertexArray *torus_va = SceneGraph::createVertexArray(0, va, 12*sizeof(f32), vertex_count, TRIANGLES, USAGE_STATIC);
	torus_va->setAttribute("Vertex", 0, 3, ATTRIB_FLOAT32);
	//cring_va->setAttribute("Texcoord", 3 * sizeof(f32), 3, ATTRIB_FLOAT32);
	torus_va->setAttribute("Normal", 3 * sizeof(f32), 3, ATTRIB_FLOAT32);
	torus_va->setAttribute("Tangent", 6 * sizeof(f32), 3, ATTRIB_FLOAT32);
	torus_va->setAttribute("Binormal", 9 * sizeof(f32), 3, ATTRIB_FLOAT32);

	/* set the index array */
	torus_va->setIndexArray(ia, sizeof(i32),3*triangle_count);

	/* va and ia are copied within createVertexArray so we can safely release their memory */
	delete va;
    delete ia;

	return torus_va;
}

VertexArray* create2DQuad(){
	struct Vertex
	{
		f32 x, y,			/* spatial coordinates */
			texx, texy;	/* texture coordinates */
	};

	/* vertex array */
	Vertex va[4];

	va[0] = { -1.0f, -1.0f, 0.0f, 0.0f };
	va[1] = { 1.0f, -1.0f, 1.0f, 0.0f };
	va[2] = { 1.0f, 1.0f, 1.0f, 1.0f };
	va[3] = { -1.0f, 1.0f, 0.0f, 1.0f };

	/* triangle def */
	struct Triangle
	{
		i32 a, b, c;		/* vertex indices */
	};

	/* create index array */
	Triangle ia[2];
	
	ia[0] = { 0, 1, 3 };
	ia[1] = { 3, 1, 2 };

	/* initialize the scene graph vertex array and set attributes (to be passed to shader) */
	VertexArray *quad_va = SceneGraph::createVertexArray(0, va, 4 * sizeof(f32), 4, TRIANGLES, USAGE_STATIC);
	quad_va->setAttribute("Vertex", 0, 2, ATTRIB_FLOAT32);
	quad_va->setAttribute("Texcoord", 2 * sizeof(f32), 2, ATTRIB_FLOAT32);

	/* set the index array */
	quad_va->setIndexArray(ia, sizeof(i32), 3*2);

	return quad_va;
}

VertexArray* createPointSprite(){
	struct Vertex
	{
		f32 x, y, z;		/* spatial coordinates */
	};

	/* vertex array */
	Vertex va;

	va = { 0.0f, 0.0f, 0.0f };

	/* initialize the scene graph vertex array and set attributes (to be passed to shader) */
	VertexArray *point_va = SceneGraph::createVertexArray(0, &va, 3 * sizeof(f32), 1, POINT_SPRITES, USAGE_STATIC);
	point_va->setAttribute("Pos", 0, 3, ATTRIB_FLOAT32);

	return point_va;
}