#include "earth.h"

void Earth::init(Node* root, Light* light)
{
	earth_spin = 1.5f;
	earth_tilt = 0.4f;
	earth_orbital_speed = -0.1f;

	earth_orbital_pivot = SceneGraph::createGroup("earth_orbital_pivot");
	earth = SceneGraph::createGeometry("sun", "scenes/sphere.obj");
	earth->scale(0.3f);

	earth_spin_pivot = SceneGraph::createGroup("earth_spin_pivot");
	earth_spin_pivot->translate(5.0f, 0.0f, 0.0f);
	earth_spin_pivot->rotateZ(earth_tilt);
	
	earth_spin_pivot->attachChild(earth);
	earth_orbital_pivot->attachChild(earth_spin_pivot);
	root->attachChild(earth_orbital_pivot);

	/* Create a shader, set its attributes and attach to sun (uncomment when reaching this point) */
	/*TODO EARTH SHADER*/
	earth_shader = SceneGraph::createShaderProgram("earth_shader", "LambertTexture", 0);
	earth_shader->setVector("LightPosition", light->getWorldPosition().vec, 3, UNIFORM_FLOAT32);
	earth_shader->setVector("LightColor", light->getColor().vec, 3, UNIFORM_FLOAT32);
	vec3f ambient = vec3f(0.0f);
	earth_shader->setVector("Ambient", ambient.vec, 3, UNIFORM_FLOAT32);
	earth->setShaderProgram(earth_shader);

	/* Create a texture from file and assign to the sun (uncomment when reaching this point) */
	earth_texture = SceneGraph::createTexture("earth_ambient_texture", "textures/earthmap_diffuse.jpg", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
	earth->setTexture("DiffuseTex", earth_texture);
}

void Earth::update(f32 timeStep)
{
	earth->rotateY( earth_spin * timeStep );
	earth_orbital_pivot->rotateY( earth_orbital_speed * timeStep);
	earth_spin_pivot->rotateY( -earth_orbital_speed * timeStep);
}