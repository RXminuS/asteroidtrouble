#ifndef RC_PROXY_H
#define RC_PROXY_H
#include "RenderChimp.h"

class Proxy : public Node {
public:
	Proxy(const char* nname, Node* proxyNode) : Node(nname, NODE_PROXY), proxyNode(proxyNode) {};
	virtual ~Proxy(){};
	virtual void drawSelf(){ proxyNode->drawSelf(); }
protected:
	Node* proxyNode;
};




#endif /* RC_NODE_H */
