#include "ParticleSystem.h"

ParticleEmitter::ParticleEmitter(const char* nname, std::shared_ptr<EmitterSettings_t> settings, int maxcount) :
Geometry(nname),
settings(settings),
maxcount(maxcount)
{
	pa = new Particle[this->maxcount];
	aliveCount = 0;
	
	//initialize the scene graph vertex array and set attributes (to be passed to shader)
	this->vertexArray = SceneGraph::createVertexArray(0, pa, 7 * sizeof(f32), this->maxcount, POINT_SPRITES, USAGE_STREAM);
	this->vertexArray->setAttribute("Pos", 0, 3, ATTRIB_FLOAT32);
	this->vertexArray->setAttribute("Vel", 3 * sizeof(f32), 3, ATTRIB_FLOAT32);
	this->vertexArray->setAttribute("Age", 6 * sizeof(f32), 1, ATTRIB_FLOAT32);
}

ParticleEmitter::~ParticleEmitter(){
	delete pa;
}

bool ParticleEmitter::update(f32 dt){
	//UPDATE EXISTING PARTICLES
	for (int i = aliveCount-1; i >= 0; --i){
		pa[i].age += dt;
		if (pa[i].age > settings->getLifetime()){
			if (i != aliveCount - 1){
				pa[i] = pa[aliveCount - 1];
			}
			--aliveCount;
		}
		else{
			vec3f newpos = vec3f(pa[i].x, pa[i].y, pa[i].z) + dt*vec3f(pa[i].vx, pa[i].vy, pa[i].vz);
			pa[i].x = newpos.x;
			pa[i].y = newpos.y;
			pa[i].z = newpos.z;
		}
	}

	//SPAWN AND KILL PARTICLES
	//Spawing Process //COMBINE WITH THE LAST PIECE
	settings->update(dt);
	if (settings->getPPS() == 0){
		if(aliveCount == 0)
			return true; //this system is dead
	}else{
		//we don't spawn more particles than capacity
		int spawncount = min(maxcount - aliveCount, settings->getPPS()*dt);
		vec3f scale = this->getScale();
		for (int i = 0; i < spawncount; ++i){
			vec3f spawnpoint = (this->getWorldMatrix()*vec4f(randflt(-1.0f, 1.0f), randflt(-1.0f, 1.0f), randflt(-1.0f, 1.0f), 1.0f)).xyz();
			vec3f vel = settings->getVelocity() * (this->getWorldMatrix()*vec4f(settings->getSpawnDir(), 0.0)).getNormalized().xyz();
			pa[aliveCount + i] = { spawnpoint.x, spawnpoint.y, spawnpoint.z, vel.x, vel.y, vel.z, 0.0f };
		}

		aliveCount += spawncount;
	}
	if (aliveCount > 0){
		this->vertexArray->setArray(pa, aliveCount);
	}
	return false;
}

 void ParticleEmitter::drawSelf(){
	 if (aliveCount > 0)
		 Geometry::drawSelf();
}

ProjectileParticleSystem::ProjectileParticleSystem(const char* nname, f32 lifetime, /*Sphere* colliders,*/ int maxcount) :
Geometry(nname),
lifetime(lifetime),
maxcount(maxcount)
{
	pa = new Particle[this->maxcount];
	alivecount = 0;

	//initialize the scene graph vertex array and set attributes (to be passed to shader)
	this->vertexArray = SceneGraph::createVertexArray(0, pa, 7 * sizeof(f32), this->maxcount, POINT_SPRITES, USAGE_STREAM);
	this->vertexArray->setAttribute("Pos", 0, 3, ATTRIB_FLOAT32);
	this->vertexArray->setAttribute("Vel", 3 * sizeof(f32), 3, ATTRIB_FLOAT32);
	this->vertexArray->setAttribute("Age", 6 * sizeof(f32), 1, ATTRIB_FLOAT32);
}

ProjectileParticleSystem::~ProjectileParticleSystem(){
	delete pa;
}

void ProjectileParticleSystem::update(f32 dt){
	//UPDATE EXISTING PARTICLES
	//We go backwards so that during the swap we have updated particles
	for (int i = alivecount - 1; i >= 0; --i){
		pa[i].age += dt;
		if (pa[i].age > lifetime){
			pa[i] = pa[alivecount - 1];
			--alivecount;
		}
		else{
			vec3f newpos = vec3f(pa[i].x, pa[i].y, pa[i].z) + dt*vec3f(pa[i].vx, pa[i].vy, pa[i].vz);
			pa[i].x = newpos.x;
			pa[i].y = newpos.y;
			pa[i].z = newpos.z;
		}
	}

	//Collision Detection!

	if (alivecount > 0){
		this->vertexArray->setArray(pa, alivecount);
		this->setVisible(true);
	}
	else{
		this->setVisible(false);
	}
}

void ProjectileParticleSystem::handleCollisions(SphereCollider* colliders, int colliderCount){
	for (int i = 0; i < this->alivecount; ++i){
		for (int j = 0; j < colliderCount; ++j){
			colliders[j].handleCollision(&vec3f(this->pa[i].x, this->pa[i].y, this->pa[i].z));
		}
	}
}

void ProjectileParticleSystem::launchProjectile(vec3f pos, vec3f vel){
	if (maxcount == alivecount)
		return;
	pa[alivecount++] = { pos.x, pos.y, pos.z, vel.x, vel.y, vel.z, 0.0f };
	this->vertexArray->setArray(pa, alivecount);
	this->setVisible(true);
}