#ifndef SUN_H
#define SUN_H

#include "RenderChimp.h"
#include "PlanetaryObject.h"

class Sun : public PlanetaryObject
{
public:
	virtual void init(Node* root, Light* light);
	virtual void update(f32 timeStep);

protected:
	Geometry *sun;
	vec3f sun_ambient;	
	ShaderProgram *sun_shader;
	Texture	*sun_texture;
	float sun_spin;
};

#endif