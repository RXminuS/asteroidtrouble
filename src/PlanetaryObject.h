#ifndef PLANETARY_OBJECT_H
#define PLANETARY_OBJECT_H

#include "RenderChimp.h"

class PlanetaryObject
{
	public:
		virtual void init(Node* root, Light* light)=0;
		virtual void update(f32 timeStep)=0;
};

#endif