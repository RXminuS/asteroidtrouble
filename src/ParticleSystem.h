#ifndef PARTICLE_SYSTEM_H
#define PARTICLE_SYSTEM_H

#include "RenderChimp.h"
#include "Collider.h"

class EmitterSettings_t{
	public: 
		virtual ~EmitterSettings_t(){};
		virtual float getPPS() = 0; //particles per second
		virtual vec3f getSpawnDir() = 0; //object space spawndirection of new particles
		virtual float getVelocity() = 0; //velocity magnitude of new particles
		virtual float getLifetime() = 0; //maximum age of a particle
		virtual void  update(f32 dt) = 0;

		virtual void setPPS(float pps) = 0;
		virtual void setLifetime(float lifetime) = 0;
		virtual void setVelocity(float vel) = 0;

};

class BoxEmitterSettings : public EmitterSettings_t{
public:
	BoxEmitterSettings(float pps, float lifetime, float vel) :
		pps(pps),
		lifetime(lifetime),
		vel(vel)
	{};
	virtual float getPPS(){ return pps; } //particles per second
	virtual vec3f getSpawnDir(){ return vec3f(0.0f, 1.0f, 0.0f); } //object space velocity vector of new particles
	virtual float getVelocity(){ return vel; } //velocity magnitude of new particles
	virtual float getLifetime(){ return lifetime; } //maximum age of a particle
	virtual void  update(f32 dt) {};

	virtual void setPPS(float pps){ this->pps = pps; }
	virtual void setLifetime(float lifetime){ this->lifetime = lifetime; }
	virtual void setVelocity(float vel){ this->vel = vel; }
protected:
	float pps;
	float lifetime;
	float vel;
};

class ParticleEmitter : public Geometry{
	public:
		ParticleEmitter(const char* nname, std::shared_ptr<EmitterSettings_t> settings, int maxcount = 1000);
		~ParticleEmitter();
		bool update(f32 dt);
		std::shared_ptr<EmitterSettings_t> getSettings(){ return settings; }
	protected:
		std::shared_ptr<EmitterSettings_t> settings;
		struct Particle{
			f32 x, y, z;
			f32 vx, vy, vz;
			f32 age;
		};
		
		int maxcount;
		int aliveCount;
		Particle* pa;

		const float randflt(float a, float b) {
			float random = ((float)rand()) / (float)RAND_MAX;
			float diff = b - a;
			float r = random * diff;
			return a + r;
		}

	virtual void drawSelf();
};

class ProjectileParticleSystem : public Geometry{
public:
	ProjectileParticleSystem(const char* nname, f32 lifetime, /*Sphere* colliders,*/ int maxcount = 100 );
	~ProjectileParticleSystem();
	void update(f32 dt);
	void handleCollisions(SphereCollider* colliders, int colliderCount);
	void launchProjectile(vec3f pos, vec3f vel);
protected:
	struct Particle{
		f32 x, y, z;
		f32 vx, vy, vz;
		f32 age;
	};
	f32 lifetime;
	int maxcount;
	int alivecount;
	Particle* pa;
};

#endif