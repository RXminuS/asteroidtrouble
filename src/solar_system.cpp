
#include "program.h"

#if defined ASSIGNMENT_1

/*
	EDA221 Introduction to Computer Graphics, 2011

	Assignment 1 - Solar System
*/

#include "RenderChimp.h"
#include "Command.h"

#include "Sun.h"
#include "Earth.h"

World	*world;				/* Scene graph root node */
Camera	*camera;			/* Scene camera */
Light	*light;				/* A light is a point in the scene which can be used for lighting calculations */


/* variables used for mouse-camera control */
vec2f	camera_rotation(0.0f, 0.0f),	/* current rotation of camera */
		mouse_prev_pos(0.0f, 0.0f);		/* previous mouse position */


/*Planetary Ojbects*/
Sun sun;
Earth earth;

void RCInit()
{
	/* Create world node */
	world = SceneGraph::createWorld("solar_system");

	/* TODO: Create camera */
	camera = SceneGraph::createCamera("main_camera");
	/* Set camera perspective projection (field-of-view, aspect ratio, near-plane. far-plane) */
	camera->setPerspectiveProjection(1.2f, 4.0f / 3.0f, 1.0f, 1000.0f);
	/* TODO: Translate the camera to (0, 1, 6) */
	camera->translate(0.0f,1.0f,6.0f);
	/* Make this camera the active camera */
	world->setActiveCamera(camera);

	world->attachChild(camera);

	/* TODO: Create light source */
    light = SceneGraph::createLight("sun_light");
	/* TODO: Set the light color to (1, 1, 1) */
    light->setColor(1.0f, 1.0f, 1.0f);
	/* TODO: Translate the light to (10, 10, 10) */
    //light->translate(10.0f, 10.0f, 10.0f);
	/* TODO: Attach the light to the world */
    world->attachChild(light);

	/* TODO: Create rest of the solar system... */
	sun.init(world, light);
	earth.init(world, light);

	/* Set clear color and depth */
	Renderer::setClearColor(vec4f(0.0f, 0.0f, 0.0f, 1.0f));
	Renderer::setClearDepth(1.0f);

    /* Enable XZ-grid and world axes */
    //Command::run("/grid 1");
    //Command::run("/axis_origin 1");
}

u32 RCUpdate()
{
	/* TODO: Perform animations for the rest of the solar system */
	float dt = Platform::getFrameTimeStep();
	sun.update(dt);
	earth.update(dt);
    
	/* Mouse & keyboard controls --> */
	/* Get the current mouse button state */
	bool *mouse = Platform::getMouseButtonState();

	/* Get mouse screen coordinates [-1, 1] */
	vec2f mouse_pos = Platform::getMousePosition();

	/* Translate mouse position and button state to camera rotations */
	if (mouse[RC_MOUSE_LEFT]) {

		/* Absolute position */

		camera_rotation.x = -mouse_pos.x * 2.0f;
		camera_rotation.y = mouse_pos.y * 2.0f;

	} else if (mouse[RC_MOUSE_RIGHT]) {

		/* Relative position */

		vec2f mouse_diff = mouse_pos - mouse_prev_pos;
		camera_rotation.x -= mouse_diff.x * 2.0f;
		camera_rotation.y += mouse_diff.y * 2.0f;
	}

	/* Perform camera rotations (pivot style) */
	camera->setRotateX(camera_rotation.y);
	camera->rotateY(camera_rotation.x);

	/* Store previous mouse screen position */
	mouse_prev_pos = mouse_pos;


	/* Get the current key state */
	bool *keys = Platform::getKeyState();

	/* Map keys to bilateral camera movement */
	f32 move = 0.0f, strafe = 0.0f;
	if (keys[RC_KEY_W])	move += 0.1f;
	if (keys[RC_KEY_S])	move -= 0.1f;
	if (keys[RC_KEY_A])	strafe -= 0.1f;
	if (keys[RC_KEY_D])	strafe += 0.1f;

	/* Apply movement to camera */
	camera->translate(camera->getLocalFront() * move);
	camera->translate(camera->getLocalRight() * strafe);

	/* Clear color and depth buffers */
	Renderer::clearColor();
	Renderer::clearDepth();

	/* tell RenderChimp to render the scenegraph */
	world->renderAll();


	return 0;
}

void RCDestroy()
{
	/* release memory allocated by the scenegraph */
	SceneGraph::deleteNode(world);
}

#endif /* ASSIGNMENT_1 */
