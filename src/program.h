
/* Inclusion guard */
#ifndef PROGRAM_H
#define PROGRAM_H

/*
	Uncomment to pick (one) active program
*/

//#define OGRE				/* demo */
//#define ASSIGNMENT_1		/* solar system */
//#define ASSIGNMENT_23		/* interpolation, tessellation, shaders */
#define ASSIGNMENT_5
#endif /* PROGRAM_H */
