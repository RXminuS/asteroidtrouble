
/* LOWPRIO: Sorry! I'm out of order */


#include "RenderChimp.h"

void PwnParse(
		Node				*target,
		const char			*filename
	)
{
	REPORT_WARNING("The .pwn-parser is currently unavailable... sorry!");
}

#if 0

#include "SceneGraph::h"
#include "Geometry.h"
#include "Quaternions.h"
#include "pwn.h"
#include "PwnParser.h"

u32 my_pwn_version = 2;

u16 load_counter = 0;

/* Warnings */
static const Result_t WRN_OPEN = {
	RESULT_WARNING, "Couldn't open .pwn file"};

static const Result_t WRN_FORMAT = {
	RESULT_WARNING, "File doesn't appear to be a .pwn file"};

static const Result_t WRN_VERSION = {
	RESULT_WARNING, "The .pwn version is not supported"};

static const Result_t WRN_TOO_MANY_MESHES = {
	RESULT_WARNING, "The number of meshes exceeds the allowed maximum"};

/*---------------------------------------------------------------------------*/

/* TODO: Remove */

static u8 read_u8(FILE *f)
{
	return (u8) fgetc(f);
}

/*---------------------------------------------------------------------------*/

static u16 read_u16(FILE *f)
{
	u16 r;

	r = ((u16) read_u8(f));
	r = r + (((u16) read_u8(f)) << 8);

	return r;
}

/*---------------------------------------------------------------------------*/

static u32 read_u32(FILE *f)
{
	u32 r;

	r = ((u32) read_u8(f));
	r += ((u32) read_u8(f)) << 8;
	r += ((u32) read_u8(f)) << 16;
	r += ((u32) read_u8(f)) << 24;

	return r;
}

/*---------------------------------------------------------------------------*/

static f32 read_f32(FILE *f)
{
	f32 v;
	u8 r[4];

	r[0] = read_u8(f);
	r[1] = read_u8(f);
	r[2] = read_u8(f);
	r[3] = read_u8(f);

	v = *((f32 *) r);

	return v;
}

/*---------------------------------------------------------------------------*/

void read_str(
		FILE				*f,
		char				*str
	)
{
	i32 i;

	while ((i = getc(f)) != '\0' && i != EOF) {
		*str = (char) i;
		str++;
	}

	*str = '\0';
}

/*---------------------------------------------------------------------------*/

void read_str(
		FILE				*f,
		char				*str,
		u32					len
	)
{
	u32 n = 0;
	i32 i;

	while (n < len && (i = getc(f)) != '\0' && i != EOF) {
		*str = (char) i;
		str++;
		n++;
	}
}

/*---------------------------------------------------------------------------*/

void PwnParse(
		Group				*target,
		const char			*filename
	)
{
	u32 i;
	u32 v;
	char buf[8192];

	u32 n_children_stack[256];
	u32 stack_ptr = 0;

	FILE *f = fopen(filename, "rb");

	Transformable *current_node = target;
	Transformable *next_node = 0;

	n_children_stack[0] = 0xFFFFFFFF;

	if (!f) {
		REPORT(WRN_OPEN);
		return;
	}

	read_str(f, buf, 4);

	if (strncmp(buf, PWN_HEADER_PREFIX, 4) != 0) {
		REPORT(WRN_FORMAT);
		return;
	}

	v = read_u32(f);

	if (my_pwn_version != PWN_HEADER_VERSION) {
		REPORT(WRN_VERSION);
		return;
	}

	for (;;) {
		u32 is_node = 0;
		read_str(f, buf, 4);

		if (strncmp(buf, PWN_VARRAY_PREFIX, 4) == 0) {

			VertexArray *va;

			u32 n_vertices;
			u32	n_values;
			u32 flags;
			u32 stride = 0;
			u32 acc = 0;
			f32 *values;

			read_str(f, buf);
			n_values = read_u32(f);
			flags = read_u32(f);

			if (flags & PWN_FLAG_VERTEX)
				stride += 3;

			if (flags & PWN_FLAG_NORMAL)
				stride += 3;

			if (flags & PWN_FLAG_TANGENT)
				stride += 3;

			if (flags & PWN_FLAG_BINORMAL)
				stride += 3;

			if (flags & PWN_FLAG_COLOR)
				stride += 3;

			if (flags & PWN_FLAG_TEXCOORDS)
				stride += 3;

			n_vertices = n_values / stride;

			values = (f32 *) malloc(n_values * sizeof(f32));

			for (i = 0; i < n_values; i++) {
				values[i] = read_f32(f);
			}

			/* TODO: Battle conflicts */

			va = SceneGraph::createVertexArray(buf, values, stride, n_vertices);

			if (flags & PWN_FLAG_VERTEX) {
				va->setAttribute(acc, 3, "Vertex");
				acc += 3;
			}

			if (flags & PWN_FLAG_NORMAL) {
				va->setAttribute(acc, 3, "Normal");
				acc += 3;
			}

			if (flags & PWN_FLAG_TANGENT) {
				va->setAttribute(acc, 3, "Tangent");
				acc += 3;
			}

			if (flags & PWN_FLAG_BINORMAL) {
				va->setAttribute(acc, 3, "Binormal");
				acc += 3;
			}

			if (flags & PWN_FLAG_COLOR) {
				va->setAttribute(acc, 3, "Color");
				acc += 3;
			}

			if (flags & PWN_FLAG_TEXCOORDS) {
				va->setAttribute(acc, 3, "Texcoord");
				acc += 3;
			}

			free(values);

		} else if (strncmp(buf, PWN_IARRAY_PREFIX, 4) == 0) {

			u32 n_indices;

			read_str(f, buf);
			n_indices = read_u32(f);

			if (n_indices > 65535) {
				/* Not yet supported! */
			} else {

				u16 *indices = (u16 *) malloc(n_indices * sizeof(u16));

				for (i = 0; i < n_indices; i++) 
					indices[i] = read_u16(f);

				/* TODO!! */
//				SceneGraph::createIndexArray("TODO", buf, indices, n_indices, TRIANGLES);

				free(indices);

			}

		} else if (strncmp(buf, PWN_MATERIAL_PREFIX, 4) == 0) {

#if 0
			MaterialPoint *m;

			u32 flags;
			rgbf ambient = rgbf(0.0f, 0.0f, 0.0f);
			rgbf diffuse = rgbf(0.1f, 0.2f, 0.8f);
			rgbf specular = rgbf(0.8f, 0.9f, 1.0f);
			f32 shininess = 1.0f;
			f32 emissive = 0.0f;
			f32 specularity = 1.0f;
			char dtex[8192];
			char stex[8192];
			char ntex[8192];

			read_str(f, buf);

			flags = read_u32(f);

			if (flags & PWN_FLAG_AMBIENT) {

				ambient.r = read_f32(f);
				ambient.g = read_f32(f);
				ambient.b = read_f32(f);

			}
			if (flags & PWN_FLAG_DIFFUSE) {

				diffuse.r = read_f32(f);
				diffuse.g = read_f32(f);
				diffuse.b = read_f32(f);

			}
			if (flags & PWN_FLAG_SPECULAR) {

				specular.r = read_f32(f);
				specular.g = read_f32(f);
				specular.b = read_f32(f);

			}
			if (flags & PWN_FLAG_SHININESS) {

				shininess = read_f32(f);

			}
			if (flags & PWN_FLAG_EMISSIVE) {

				emissive = read_f32(f);

			}
			if (flags & PWN_FLAG_SPECULARITY) {

				specularity = read_f32(f);

			}
			if (flags & PWN_FLAG_DIFFUSE_TEXTURE) {

				read_str(f, dtex);

			}
			if (flags & PWN_FLAG_SPECULARITY_TEXTURE) {

				read_str(f, stex);

			}
			if (flags & PWN_FLAG_NORMAL_TEXTURE) {

				read_str(f, ntex);

			}

			m = (MaterialPoint *) SceneGraph::createMaterial(MATERIAL_POINT, buf);
			m->ambient = ambient;
			m->diffuse = diffuse;
			m->specular = specular;
			m->emissive = emissive;
			m->shininess = shininess;
			m->specularLevel = specularity;
#endif

		} else if (strncmp(buf, PWN_TEXTURE_PREFIX, 4) == 0) {

			read_str(f, buf);

			SceneGraph::createTexture(buf, true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);

		} else if (strncmp(buf, PWN_MESH_PREFIX, 4) == 0) {

			VertexArray *va;
			IndexArray *ia;
//			Material *mat;

			char tmp[8192];

			read_str(f, buf);

			read_str(f, tmp);
//			mat = SceneGraph::getMaterial(tmp);

			read_str(f, tmp);
			va = SceneGraph::getVertexArray(tmp);

			read_str(f, tmp);
			ia = SceneGraph::getIndexArray(tmp);

			next_node = SceneGraph::createGeometry(buf, va, ia, mat, false);

			is_node = 1;

		} else if (strncmp(buf, PWN_GROUP_PREFIX, 4) == 0) {

			read_str(f, buf);

			next_node = SceneGraph::createGroup(buf);

			is_node = 1;

		} else if (strncmp(buf, PWN_CAMERA_PREFIX, 4) == 0) {

			Camera *cam;

			f32 fov;
			f32 ratio;
			f32 nearp;
			f32 farp;

			read_str(f, buf);

			fov = read_f32(f);
			ratio = read_f32(f);
			nearp = read_f32(f);
			farp = read_f32(f);

			cam = SceneGraph::createCamera(buf);
			ratio = 1.5f;

			cam->setPerspectiveProjection(fov, ratio, nearp, farp);

			next_node = cam;

			is_node = 1;

		} else if (strncmp(buf, PWN_LIGHT_PREFIX, 4) == 0) {

			read_str(f, buf);

			rgbf rgb;
			f32 intensity;

			rgb.r = read_f32(f);
			rgb.g = read_f32(f);
			rgb.b = read_f32(f);
			intensity = read_f32(f);
			read_f32(f); // radius

			next_node = SceneGraph::createLight(buf, rgb, intensity);

			is_node = 1;

		} else if (strncmp(buf, PWN_END_FILE, 4) == 0) {

			break;

		}

		if (is_node) {

			u32 n_children;
			u32 flags;
			vec3f t = vec3f(0.0f, 0.0f, 0.0f);
			quat r = quat(0.0f, 0.0f, 0.0f, 1.0f);
			vec3f s = vec3f(1.0f, 1.0f, 1.0f);

			flags = read_u32(f);

			if (flags & PWN_FLAG_TRANSLATION) {
				t.x = read_f32(f);
				t.y = read_f32(f);
				t.z = read_f32(f);
			}
			if (flags & PWN_FLAG_ROTATION) {
				r.x = read_f32(f);
				r.y = read_f32(f);
				r.z = read_f32(f);
				r.w = read_f32(f);
			}
			if (flags & PWN_FLAG_SCALE) {
				s.x = read_f32(f);
				s.y = read_f32(f);
				s.z = read_f32(f);
			}

			next_node->setTranslate(t);
			next_node->setRotate(r);
			next_node->setScale(s);

			/* Handle hierarchy */
			current_node->attachChild(next_node);

			n_children_stack[stack_ptr]--;

			n_children = read_u32(f);

			if (n_children > 0) {

				stack_ptr++;
				n_children_stack[stack_ptr] = n_children;

				current_node = next_node;

			}

//			if (n_children_stack[stack_ptr] == 0 && stack_ptr > 0)
//				stack_ptr--;

			while (n_children_stack[stack_ptr] == 0 && stack_ptr > 0) {
				current_node = (Transformable *) current_node->getParent();
				stack_ptr--;
			}

		}

	}

	load_counter++;

	fclose(f);
}

/*---------------------------------------------------------------------------*/

#endif

