
#include "program.h"

#if defined ASSIGNMENT_23

/*
	EDA221 Introduction to Computer Graphics, 2011

	Assignment 2 - Parametric Shapes
*/

#include "RenderChimp.h"
#include "Command.h"
#include "ParametricShapes.h"
#include "Interpolation.h"

World	*world;				/* Scene graph root node */
Camera	*camera;			/* Scene camera */
Group	*camera_pivot;		/* A group is an empty, transformable node */
Light	*light;				/* A light is a point in the scene which can be used for lighting calculations */

Geometry		*shape;	/* */
Geometry		*shape_cubic;
Geometry		*shape_ratmull;
VertexArray		*shape_va;

vec3f			ambient(0.3f, 0.3f, 0.3f);	/* Ambient color for the sun (never gets dark) */

ShaderProgram	*shape_shader;		/* To add textures we need a shader program (vertex + fragment shader) */
ShaderProgram	*phong_shader;
ShaderProgram	*bump_shader;
ShaderProgram	*reflection_shader;
ShaderProgram	*skybox_shader;

Texture			*diffuse_texture;
Texture			*bump_texture;

CubeMap			*cube_map;

float			spin = 0.2f;	/* Angular velocity of the sun (radians/s) */

f64				elapsed_time = 0.0f; 
/* variables used for mouse-camera control */
vec2f	camera_rotation(0.0f, 0.0f),	/* current rotation of camera */
		mouse_prev_pos(0.0f, 0.0f);		/* previous mouse position */

vec3f interpolation_points[10];

void RCInit()
{
	

	world = SceneGraph::createWorld("solar_system");

	/* Camera */
	camera = SceneGraph::createCamera("cam1");
	camera->setPerspectiveProjection(1.2f, 4.0f / 3.0f, 1.0f, 1000.0f);
	camera->translate(0.0f, 0.0f, 10.0f);
	world->setActiveCamera(camera);

	/* Create pivot */
	camera_pivot = SceneGraph::createGroup("camera_pivot");
	camera_pivot->attachChild(camera);
	world->attachChild(camera_pivot);
	
	/* light */
	light = SceneGraph::createLight("light1");
	light->setColor(1.0f, 1.0f, 1.0f);

	/* Create a shader and set its attributes */
	shape_shader = SceneGraph::createShaderProgram("shader", "Normals", 0);

	/* Create vertex array for circle ring */
	shape_va = createCircleRing(4, 60, 0.5, 1);
    shape = SceneGraph::createGeometry("saturn_rings", shape_va, false);
	shape->translate(2.5f,0,0);
	world->attachChild(shape);
	shape->setShaderProgram(shape_shader);
    
	shape_va = createSphere(15,40,1);
	shape = SceneGraph::createGeometry("sphere", shape_va, false);
	world->attachChild(shape);
	shape->setShaderProgram(shape_shader);
    
	shape_va = createTorus(20,20,0.75,0.25);
	shape = SceneGraph::createGeometry("torus", shape_va, false);
	shape->translate(-2.5f,0,0);
	world->attachChild(shape);
	shape->setShaderProgram(shape_shader);

	shape_va = createSphere(3,3,0.2);
	shape_cubic = SceneGraph::createGeometry("sphere_cubic", shape_va, false);
	shape_cubic->attachChild(light);
	world->attachChild(shape_cubic);
	shape_cubic->setShaderProgram(shape_shader);

	shape_va = createSphere(10,10,0.05);
	for(int i = 0; i < 10; ++i)
	{
		interpolation_points[i] = vec3f((rand()%100-50.0f)/10.0f,(rand()%100-50.0f)/5.0f,(rand()%100-50.0f)/10.0f);
		shape = SceneGraph::createGeometry("control-point",shape_va,true);
		shape->translate(interpolation_points[i]);
		world->attachChild(shape);
		shape->setShaderProgram(shape_shader);
	}

	//Generate Shader Demo Material
	/* Phong Sphere */
	phong_shader = SceneGraph::createShaderProgram("phong_shader", "Phong", 0);
	phong_shader->setVector("LightPosition", light->getWorldPosition().vec, 3, UNIFORM_FLOAT32);
	phong_shader->setVector("Ambient", ambient.vec, 3, UNIFORM_FLOAT32);
	phong_shader->setVector("ka", vec3f(0.0f).vec, 3, UNIFORM_FLOAT32);
	phong_shader->setVector("kd", vec3f(0.8f,0.6f,0.2f).vec, 3, UNIFORM_FLOAT32);
	phong_shader->setVector("ks", vec3f(0.3f).vec, 3, UNIFORM_FLOAT32);
	phong_shader->setScalar("shininess", 100.0f, UNIFORM_FLOAT32);

	shape_va = createSphere(20,20,1);
	shape = SceneGraph::createGeometry("sphere_phong", shape_va, false);
	shape->translate(0,3,0);

	world->attachChild(shape);
	shape->setShaderProgram(phong_shader);

	/* Textured BumpMapped Sphere */
	bump_shader = SceneGraph::createShaderProgram("bump_shader", "PhongTextureBump", 0);
	bump_shader->setVector("LightPosition", light->getWorldPosition().vec, 3, UNIFORM_FLOAT32);
	bump_shader->setVector("Ambient", ambient.vec, 3, UNIFORM_FLOAT32);
	bump_shader->setVector("ka", vec3f(0.0f).vec, 3, UNIFORM_FLOAT32);
	bump_shader->setVector("ks", vec3f(1.0f).vec, 3, UNIFORM_FLOAT32);
	bump_shader->setScalar("shininess", 25.0f, UNIFORM_FLOAT32);

	shape_va = createSphere(20,20,1);
	shape = SceneGraph::createGeometry("sphere_bump", shape_va, false);
	shape->translate(0,3,-3);

	diffuse_texture = SceneGraph::createTexture("diffuse_texture", "textures/Fieldstone_diffuse.jpg", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
	bump_texture = SceneGraph::createTexture("bump_texture", "textures/Fieldstone_bump.jpg", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
	
	world->attachChild(shape);
	shape->setShaderProgram(bump_shader);
	shape->setTexture("DiffuseTex", diffuse_texture);
	shape->setTexture("BumpTex", bump_texture);

	/* Reflection Sphere */
	reflection_shader = SceneGraph::createShaderProgram("reflection_shader", "Reflection", 0);

	shape_va = createSphere(20,20,1);
	shape = SceneGraph::createGeometry("sphere_reflect", shape_va, false);
	shape->translate(0,3,3);

	cube_map = SceneGraph::createCubeMap("cube_map", "textures/snow_cubemap/snow_posx.jpg", "textures/snow_cubemap/snow_negx.jpg", "textures/snow_cubemap/snow_posy.jpg", "textures/snow_cubemap/snow_negy.jpg", "textures/snow_cubemap/snow_negz.jpg", "textures/snow_cubemap/snow_posz.jpg");
	
	world->attachChild(shape);
	shape->setShaderProgram(reflection_shader);
	shape->setCubeMap("CubeMapTex", cube_map);
	/* Skybox Sphere */
	skybox_shader = SceneGraph::createShaderProgram("skybox_shader", "Skybox", 0);

	shape_va = createSphere(5,5,1000);
	shape = SceneGraph::createGeometry("sphere_skybox", shape_va, false);

	CubeMap *skybox = SceneGraph::createCubeMap("skybox", "textures/snow_cubemap/snow_posx.jpg", "textures/snow_cubemap/snow_negx.jpg", "textures/snow_cubemap/snow_posy.jpg", "textures/snow_cubemap/snow_negy.jpg", "textures/snow_cubemap/snow_negz.jpg", "textures/snow_cubemap/snow_posz.jpg");
	
	camera->attachChild(shape);
	shape->setShaderProgram(skybox_shader);
	shape->setCubeMap("SkyboxTex", skybox);
	
	skybox_shader->getRenderState()->disableCulling();

	/* Set clear color and depth */
	Renderer::setClearColor(vec4f(0.1f, 0.1f, 0.1f, 1.0f));
	Renderer::setClearDepth(1.0f);

    /* Enable XZ-grid and world axes */
    Command::run("/grid 1");
    Command::run("/axis_origin 1");

	
	
}

f32 x(f64 t)
{
	return (f32)t - floor(t);
}

vec3f interpolate(vec3f ip[], int count, f32 x, bool catmull = false)
{
	int bpi = floor(x*count);
	int bpip = floor((float)((bpi+1)%count));

	float localx = x*count-floor(x*count);

	if(catmull)
	{
		int bpipp = floor((float)((bpi+2)%count));
		int bpim = (bpi-1 < 0)?count-1:bpi-1;
		return evalCatmullRom(ip[bpim],ip[bpi],ip[bpip],ip[bpipp],1,localx);
	}
	return evalLERP(ip[bpi],ip[bpip], localx);
}

u32 RCUpdate()
{
	elapsed_time += Platform::getFrameTimeStep()/50;
	shape_cubic->setTranslate(interpolate(interpolation_points,10,x(elapsed_time), true));//evalCatmullRom(vec3f(0,2,0),vec3f(0,4,0),vec3f(0,2,-2),vec3f(0,2,0),1,x(elapsed_time)));
	
	bump_shader->setVector("LightPosition", light->getWorldPosition().vec, 3, UNIFORM_FLOAT32);
	phong_shader->setVector("LightPosition", light->getWorldPosition().vec, 3, UNIFORM_FLOAT32);

	/* Mouse & keyboard controls --> */
	RenderTarget *target;

	/* Get the current mouse button state */
	bool *mouse = Platform::getMouseButtonState();

	/* Get mouse screen coordinates [-1, 1] */
	vec2f mouse_pos = Platform::getMousePosition();

	/* Translate mouse position and button state to camera rotations */
	if (mouse[RC_MOUSE_LEFT]) {

		/* Absolute position */

		camera_rotation.x = -mouse_pos.x * 2.0f;
		camera_rotation.y = mouse_pos.y * 2.0f;

	} else if (mouse[RC_MOUSE_RIGHT]) {

		/* Relative position */

		vec2f mouse_diff = mouse_pos - mouse_prev_pos;
		camera_rotation.x -= mouse_diff.x * 2.0f;
		camera_rotation.y += mouse_diff.y * 2.0f;
	}

	/* Perform camera rotations */
	camera_pivot->setRotateX(camera_rotation.y);
	camera_pivot->rotateY(camera_rotation.x);

	/* Store previous mouse screen position */
	mouse_prev_pos = mouse_pos;

	/* Get the current key state */
	bool *keys = Platform::getKeyState();

	/* Map keys to bilateral camera movement */
	f32 move = 0.0f, strafe = 0.0f;
	if (keys[RC_KEY_W])	move += 0.1f;
	if (keys[RC_KEY_S])	move -= 0.1f;
	if (keys[RC_KEY_A])	strafe -= 0.1f;
	if (keys[RC_KEY_D])	strafe += 0.1f;

	/* Apply movement to camera */
	camera->translate(camera->getLocalFront() * move);
	camera->translate(camera->getLocalRight() * strafe);

	/* Clear color and depth buffers */
	Renderer::clearColor();
	Renderer::clearDepth();

	/* Tell RenderChimp to render the scenegraph */
	world->renderAll();
	
	return 0;
}

void RCDestroy()
{
	// release memory allocated by the scenegraph
	SceneGraph::deleteNode(world);
}

#endif /* ASSIGNMENT_2 */

