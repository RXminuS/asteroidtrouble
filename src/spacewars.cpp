#include "program.h"

#if defined ASSIGNMENT_5
#include <string>
#include "RenderChimp.h"
#include "PerlinNoise.h"
#include "Command.h"
#include "ParametricShapes.h"
#include "Interpolation.h"
#include "ParticleSystem.h"
#include "Collider.h"

//Gameplay Constants
const f32 k_earthRadius      = 20000.0f;
const f32 k_atmosphereRadius = 20500.0f;
const vec3f maxSpeed     = vec3f(10.0f, 10.0f, fPI);
const vec3f cameraOffset = vec3f(0.0f, 0.75f, 5.0f);
const vec3f g_spacestationLoc = vec3f(-sinf(fPI / 20.0f)*26000.0f, 0.0f, -cosf(fPI / 20.0)*26000.0f);

const PerlinNoise perlin(0.9f, 8);

//Rendering Constants
const u32 auxBuffWidth    = RC_DEFAULT_DISPLAY_WIDTH / 2;
const u32 auxBuffHeight   = RC_DEFAULT_DISPLAY_HEIGHT / 2;
const u32 lumaBuffSize    = 256;
const f32 nearplane       = 0.01f;
const f32 k_farplane      = 100000.0f;
const f32 fcoef           = 2.0f / log2(k_farplane + 1.0f);
const f32 aspect          = f32(RC_DEFAULT_DISPLAY_WIDTH) / f32(RC_DEFAULT_DISPLAY_HEIGHT);
vec4f bloomScale          = vec4f(0.01f, 0.01);
vec4f bloomBias           = vec4f(-5.0, -5.0);

//Gameplay Variables
vec3f playerSpeed      = vec3f(0.0f, 0.0f, 0.0f); //forward/backward, up/down, rotationalSpeed
vec2f playerRotation   = vec2f(0.0f, 0.0f); //pitch, roll
vec2f cameraRotation   = vec2f(0.0f, 0.0f);
bool cameraLocked      = true;
bool cameraLockToggled = false;

//Rendering Variables
u32 currentAdaptLuma = 0;
f32 adaptionRate = 1.5f;

//Objecst
World*			g_mainWorld;
World*			g_particleWorld;
World*			g_UIWorld;

Camera*			g_playerCam;
Camera*			g_UICam;
Group*			g_playerGroup;
Group*			g_cameraPivot;

Geometry*		g_earthGeom;
Geometry*		g_atmosphereGeom;
Geometry*		g_skyboxGeom;
Geometry*		g_spaceship01Geom;
Geometry*		g_spacestationGeom;
Geometry*		g_meteorGeom;
Geometry*		g_meteorFractureGeom;
Geometry*		g_HUDSpinnerGeom;
Geometry*		g_HUDGeom;
Geometry*		g_fsQuadGeom;
Geometry*		rocketGeometry;
Geometry*		enemyGeometry;
Geometry*		g_explosionGeometry;
Geometry*		g_sunGeom;

SphereCollider*  enemyCollider;

Proxy*			particleProxyMain;
Proxy*			particleProxySide1;
Proxy*			particleProxySide2;

Light			*sun;

ShaderProgram*	g_earthShader;
ShaderProgram*	g_atmosphereShader;
ShaderProgram*	g_skyboxShader;
ShaderProgram*	g_phongTextureShader;
ShaderProgram*	g_lambertTextureShader;
ShaderProgram*	g_UIQuadShader;
ShaderProgram*	g_projectileShader;
ShaderProgram*	g_particleShader;
ShaderProgram*	g_testShader;

ShaderProgram* g_explosionShader;
ShaderProgram* g_postprocessShader; //("shaders/basic.vs.glsl", "shaders/postprocess.fs.glsl")
ShaderProgram* g_lumaShader; //"shaders/basic.vs.glsl", "shaders/lum.fs.glsl"
ShaderProgram* g_lumaMipMapShader; //"shaders/basic.vs.glsl", "shaders/avgminmax.fs.glsl");
ShaderProgram* g_lumaAdaptShader; //"shaders/basic.vs.glsl", "shaders/adaptlum.fs.glsl"
ShaderProgram* g_gaussBlurShader; //"shaders/basic.vs.glsl", "shaders/gauss1d.fs.glsl"
ShaderProgram* g_scaleBiasShader; //"shaders/basic.vs.glsl", "shaders/scalebias.fs.glsl"

RenderTarget*	rt_HDR;
RenderTarget*	rt_AuxA;
RenderTarget*	rt_AuxB;
RenderTarget*	rt_AuxC;
RenderTarget**	rt_Luma;
RenderTarget*	rt_AdaptLuma[2];

Texture			*g_earthDiffuseTex;
Texture			*g_earthNormalTex;
Texture			*g_earthSpecularTex;

Texture*		g_spaceship01DiffuseTex;
Texture*		g_spacestationDiffuseTex;
Texture*		g_meteorDiffuseTex;
Texture*		g_HUDTex;
Texture*		g_HUDSpinnerTex;
Texture*		g_pointParticleTex;
Texture*		g_starParticleTex;
Texture*		g_explosionDispl;
Texture*		g_explosionRamp;
CubeMap*		g_skyboxCubemap;

ParticleEmitter* g_playerEngineParticles[3];
ProjectileParticleSystem* g_playerGunProjectiles;

bool fired = false;
float rocketSpeed = 0.0f;


class ExplosionManager{
public:
	static void init(){
		aliveCount = 0;
	}

	static void spawnExplosion(vec3f pos){
		if (aliveCount < maxCount){
			explosions[aliveCount++] = new Explosion(pos);
		}
	}

	static void updateExplosions(f32 dt){
		for (int i = aliveCount-1; i >= 0; --i){
			explosions[i]->m_age += dt;
			if (explosions[i]->m_age > m_maxAge){
				delete explosions[i];
				if (i != aliveCount - 1){
					explosions[i] = explosions[aliveCount - 1];
				}
				--aliveCount;
				continue;
			}
			float ndt = (explosions[i]->m_age - floor(explosions[i]->m_age));
			float r = sinf((ndt /* loopduration*/)* (2 * fPI)) * 0.5f + 0.25f;
			float g = sinf((ndt /* loopduration*/ + 0.33333333f) * 2 * fPI) * 0.5f + 0.25f;
			float b = sinf((ndt /* loopduration*/ + 0.66666667f) * 2 * fPI) * 0.5f + 0.25f;
			float correction = 1 / (r + g + b);
			r *= correction;
			g *= correction;
			b *= correction;

			explosions[i]->m_g->setScale(pow(ndt, 0.2f) + ndt);
			//explosionShader->setScalar("displacement", 2.0f*(1.0f-0.5f*ndt), UNIFORM_FLOAT32);
			explosions[i]->m_g->setVector("channelFactor", &vec3f(r, g, b), 3, UNIFORM_FLOAT32);
			explosions[i]->m_g->setVector("range", &vec3f(0.0f, 0.3f + 4.0f*pow(ndt, 3), 0.0f), 3, UNIFORM_FLOAT32);
			explosions[i]->m_g->setScalar("clipRange", 0.7f - pow(ndt, 8.0f), UNIFORM_FLOAT32);
		}
	}

private:
	struct Explosion{
		Explosion(vec3f pos){
			std::string name("explosionGeom");
			name.append(std::to_string(rand()));
			m_g = (Geometry*)g_explosionGeometry->duplicate(const_cast<char*>(name.c_str()), true);
			m_g->setTranslate(pos);
			g_mainWorld->attachChild(m_g);
			m_age = 0.0f;
		}

		~Explosion(){
			g_mainWorld->detachChild(m_g);
			SceneGraph::deleteNode(m_g);
		}

		Geometry* m_g;
		f32 m_age;
	};

	static const f32 m_maxAge;
	static const u32 maxCount = 30;
	static Explosion* explosions[];
	static u32 aliveCount;
};
const float ExplosionManager::m_maxAge = 1.0f;
ExplosionManager::Explosion* ExplosionManager::explosions[ExplosionManager::maxCount];
u32 ExplosionManager::aliveCount = 0;

class MeteorManager{
public:
	static void init(){
		
	}

	static u32 getAliveCount(){
		return aliveCount;
	}

	static void spawnRandMeteor(f32 radius){
		if (aliveCount < maxCount){
			vec3f spawnpos = attractionPoint + radius*vec3f(randflt(-1.0f, 1.0f), randflt(-1.0f, 1.0f), randflt(-1.0f, 1.0f)).getNormalized();
			vec3f vel = (attractionPoint - spawnpos).getNormalized()*5.0f; //TODO: make adjustable speed
			vec3f rotvel = vec3f(randflt(-1.0f, 1.0f), randflt(-1.0f, 1.0f), randflt(-1.0f, 1.0f)).getNormalized()*fPI; //TODO: make adjustable speed
			meteors[aliveCount++] = new Meteor(spawnpos, vel, rotvel);
		}
	}

	static void destroyMeteor(u32 meteorId){
		if (meteorId < aliveCount){
			ExplosionManager::spawnExplosion(meteors[meteorId]->m_g->getWorldPosition());
			delete meteors[meteorId];
			if (meteorId != aliveCount - 1){
				meteors[meteorId] = meteors[aliveCount - 1];
				
			}
			--aliveCount;
		}
	}

	static void updateMeteors(f32 dt){
		for (int i = 0; i < aliveCount; ++i){
			vec3f dx = meteors[i]->m_vel*dt;
			f32 dr = meteors[i]->m_rotvel*dt;
			meteors[i]->m_g->translate(dx);
			meteors[i]->m_g->rotate(dr, meteors[i]->m_rotaxis);
		}
	}

	static i32 intersects(vec3f& p){
		AABox boundingBox;
		for (int i = 0; i < aliveCount; ++i){
			 meteors[i]->m_g->getWorldBoundingBox(&boundingBox);
			 if (boundingBox.encloses(&p))
				 return i;
		}
		return -1;
	}

	static i32 intersects(AABox& aab){
		AABox boundingBox;
		for (int i = 0; i < aliveCount; ++i){
			meteors[i]->m_g->getWorldBoundingBox(&boundingBox);
			if (boundingBox.intersect(&aab))
				return i;
		}
		return -1;
	}
private:
	static const float randflt(float a, float b) {
		float random = ((float)rand()) / (float)RAND_MAX;
		float diff = b - a;
		float r = random * diff;
		return a + r;
	}

	struct Meteor{
		Meteor(vec3f pos, vec3f vel, vec3f rotvel){
			std::string name("meteorGeom");
			name.append(std::to_string(rand()));
			m_g = (Geometry*)g_meteorGeom->duplicate(const_cast<char*>(name.c_str()), true);
			m_g->setTranslate(pos);
			g_mainWorld->attachChild(m_g);

			m_rotaxis = rotvel.getNormalized();
			m_rotvel = rotvel.length();
			m_vel = vel;
		}

		~Meteor(){
			g_mainWorld->detachChild(m_g);
			SceneGraph::deleteNode(m_g);
		}

		Geometry* m_g;
		vec3f m_vel;
		vec3f m_rotaxis;
		float m_rotvel;
	};
	static const u32 maxCount = 30;
	static Meteor* meteors[];
	static u32 aliveCount;
	static vec3f attractionPoint;
};
MeteorManager::Meteor* MeteorManager::meteors[MeteorManager::maxCount];
u32 MeteorManager::aliveCount = 0;
vec3f MeteorManager::attractionPoint = g_spacestationLoc;

class ProjectileManager{
public:
	static void init(){
	}

	static void spawnProjectile(vec3f pos, vec3f vel){
		if (aliveCount < maxCount){
			projectiles[aliveCount++] = new Projectile(pos, vel);
		}
	}

	static void updateProjectiles(f32 dt){
		for (int i = aliveCount - 1; i >= 0; --i){
			projectiles[i]->m_age += dt;
			if (projectiles[i]->m_pe->update(dt)){
				delete projectiles[i];
				if (i != aliveCount - 1){
					projectiles[i] = projectiles[aliveCount - 1];
				}
				--aliveCount;
				continue;
			}
			if (!projectiles[i]->m_dying){
				i32 hit = MeteorManager::intersects(projectiles[i]->m_pe->getWorldPosition());
				if (hit != -1){
					MeteorManager::destroyMeteor(hit);
					projectiles[i]->m_dying = true;
					projectiles[i]->m_pe->getSettings()->setPPS(0.0f);
				}
				else if (projectiles[i]->m_age > m_maxAge){
					projectiles[i]->m_dying = true;
					projectiles[i]->m_pe->getSettings()->setPPS(0.0f);
				}
				else{
					vec3f dx = projectiles[i]->m_vel*dt;
					projectiles[i]->m_pe->translate(dx);
				}
			}
		}
	}

private:
	struct Projectile{
		Projectile(vec3f pos, vec3f vel){
			m_pe = new ParticleEmitter("projectile", std::shared_ptr<EmitterSettings_t>(new BoxEmitterSettings(800.0f, 3.0f, 0.0f)), 2500);
			m_pe->setScale(vec3f(0.1f, 0.1f, 0.1f));
			m_pe->setTranslate(pos);
			m_pe->setShaderProgram(g_projectileShader);
			g_particleWorld->attachChild(m_pe);
			m_age = 0.0f;
			m_vel = vel;
			m_dying = false;
		}

		~Projectile(){
			g_particleWorld->detachChild(m_pe);
			delete m_pe;
		}

		ParticleEmitter* m_pe;
		vec3f m_vel;
		f32 m_age;
		bool m_dying;
	};

	static const f32 m_maxAge;
	static const u32 maxCount = 5;
	static Projectile* projectiles[];
	static u32 aliveCount;
};
const float ProjectileManager::m_maxAge = 7.0f;
ProjectileManager::Projectile* ProjectileManager::projectiles[ProjectileManager::maxCount];
u32 ProjectileManager::aliveCount = 0;

void testHandler(vec3f* point){
	Console::log("Collision detected!");
}

void loadShaders(){
	Console::log("Loading Shaders...");
	g_earthShader              = SceneGraph::createShaderProgram("earthShader", "Earth", 0);
	g_atmosphereShader         = SceneGraph::createShaderProgram("atmosphereShader", "Atmosphere", 0);
	g_skyboxShader             = SceneGraph::createShaderProgram("skyboxShader", "Skybox", 0);
	g_phongTextureShader       = SceneGraph::createShaderProgram("phongTextureShader", "PhongTexture", 0);
	g_lambertTextureShader     = SceneGraph::createShaderProgram("lambertTextureShader", "LambertTexture", 0);
	g_projectileShader         = SceneGraph::createShaderProgram("projectileShader", "PointSpriteShader", 0);
	g_particleShader           = SceneGraph::createShaderProgram("particleShader", "PointSpriteShader", 0);
	g_explosionShader          = SceneGraph::createShaderProgram("explosionShader", "Explosion", 0);
	g_UIQuadShader             = SceneGraph::createShaderProgram("UIQuadShader", "UIQuad", 0);
	g_postprocessShader        = SceneGraph::createShaderProgram("postprocessShader", "Postprocess", 0);
	g_lumaShader               = SceneGraph::createShaderProgram("lumaShader", "Luma", 0);
	g_lumaAdaptShader          = SceneGraph::createShaderProgram("lumaAdaptShader", "AdaptLuma", 0);
	g_lumaMipMapShader         = SceneGraph::createShaderProgram("lumaMipMapShader", "AvgMinMax", 0);
	g_gaussBlurShader          = SceneGraph::createShaderProgram("gaussBlurShader", "Gauss1D", 0);
	g_scaleBiasShader          = SceneGraph::createShaderProgram("scaleBiasShader", "ScaleBias", 0);
	g_testShader = SceneGraph::createShaderProgram("testShader", "Test", 0);
}

void setShaderConstants(){
	f32 wavelengths[3];
	wavelengths[0] = powf(0.650f, 4.0f);
	wavelengths[1] = powf(0.570f, 4.0f);
	wavelengths[2] = powf(0.475f, 4.0f);
	float fESun = 20.0f;
	float fKr = 0.0025f;
	float fKm = 0.0010f;
	g_earthShader->setVector("v3SpecularColor", vec3f(0.75f, 0.7f, 0.6f).vec, 3, UNIFORM_FLOAT32);
	g_earthShader->setScalar("fSpecularPower", 15.0f, UNIFORM_FLOAT32);
	g_earthShader->setVector("v3LightDir", sun->getWorldPosition().vec, 3, UNIFORM_FLOAT32);
	g_earthShader->setVector("v3InvWavelength", vec3f(1.0f / wavelengths[0], 1.0f / wavelengths[1], 1.0f / wavelengths[2]).vec, 3, UNIFORM_FLOAT32);
	g_earthShader->setScalar("fInnerRadius", k_earthRadius, UNIFORM_FLOAT32);
	g_earthShader->setScalar("fInnerRadius2", k_earthRadius*k_earthRadius, UNIFORM_FLOAT32);
	g_earthShader->setScalar("fOuterRadius", k_atmosphereRadius, UNIFORM_FLOAT32);
	g_earthShader->setScalar("fOuterRadius2", k_atmosphereRadius*k_atmosphereRadius, UNIFORM_FLOAT32);
	g_earthShader->setScalar("fKrESun", fKr*fESun, UNIFORM_FLOAT32);
	g_earthShader->setScalar("fKmESun", fKm*fESun, UNIFORM_FLOAT32);
	g_earthShader->setScalar("fKr4PI", fKr*4.0f*fPI, UNIFORM_FLOAT32);
	g_earthShader->setScalar("fKm4PI", fKm*4.0f*fPI, UNIFORM_FLOAT32);
	g_earthShader->setScalar("fScale", 1.0f / (k_atmosphereRadius - k_earthRadius), UNIFORM_FLOAT32);
	g_earthShader->setScalar("fScaleOverScaleDepth", (1.0f / (k_atmosphereRadius - k_earthRadius)) / 0.25f, UNIFORM_FLOAT32);
	g_earthShader->setScalar("fG", -0.990f, UNIFORM_FLOAT32);
	g_earthShader->setScalar("fG2", -0.990f*-0.990f, UNIFORM_FLOAT32);
	g_earthShader->setScalar("fcoef", fcoef, UNIFORM_FLOAT32);

	g_atmosphereShader->setVector("v3LightDir", sun->getWorldPosition().vec, 3, UNIFORM_FLOAT32);
	g_atmosphereShader->setVector("v3InvWavelength", vec3f(1.0f / wavelengths[0], 1.0f / wavelengths[1], 1.0f / wavelengths[2]).vec, 3, UNIFORM_FLOAT32);
	g_atmosphereShader->setScalar("fInnerRadius", k_earthRadius, UNIFORM_FLOAT32);
	g_atmosphereShader->setScalar("fInnerRadius2", k_earthRadius*k_earthRadius, UNIFORM_FLOAT32);
	g_atmosphereShader->setScalar("fOuterRadius", k_atmosphereRadius, UNIFORM_FLOAT32);
	g_atmosphereShader->setScalar("fOuterRadius2", k_atmosphereRadius*k_atmosphereRadius, UNIFORM_FLOAT32);
	g_atmosphereShader->setScalar("fKrESun", fKr*fESun, UNIFORM_FLOAT32);
	g_atmosphereShader->setScalar("fKmESun", fKm*fESun, UNIFORM_FLOAT32);
	g_atmosphereShader->setScalar("fKr4PI", fKr*4.0f*fPI, UNIFORM_FLOAT32);
	g_atmosphereShader->setScalar("fKm4PI", fKm*4.0f*fPI, UNIFORM_FLOAT32);
	g_atmosphereShader->setScalar("fScale", 1.0f / (k_atmosphereRadius - k_earthRadius), UNIFORM_FLOAT32);
	g_atmosphereShader->setScalar("fScaleOverScaleDepth", (1.0f / (k_atmosphereRadius - k_earthRadius)) / 0.25f, UNIFORM_FLOAT32);
	g_atmosphereShader->setScalar("fG", -0.990f, UNIFORM_FLOAT32);
	g_atmosphereShader->setScalar("fG2", -0.990f*-0.990f, UNIFORM_FLOAT32);
	g_atmosphereShader->setScalar("fcoef", fcoef, UNIFORM_FLOAT32);
	g_atmosphereShader->getRenderState()->enableCulling(CULL_FRONT);

	g_skyboxShader->setScalar("fcoef", fcoef, UNIFORM_FLOAT32);
	g_skyboxShader->getRenderState()->disableDepthWrite();
	g_skyboxShader->getRenderState()->disableCulling();
	g_skyboxShader->getRenderState()->disableDepthTest();

	g_phongTextureShader->setVector("LightPosition", sun->getWorldPosition().vec, 3, UNIFORM_FLOAT32);
	g_phongTextureShader->setScalar("fcoef", fcoef, UNIFORM_FLOAT32);

	g_lambertTextureShader->setVector("LightPosition", sun->getWorldPosition().vec, 3, UNIFORM_FLOAT32);
	g_lambertTextureShader->setScalar("fcoef", fcoef, UNIFORM_FLOAT32);

	g_projectileShader->setScalar("fcoef", fcoef, UNIFORM_FLOAT32);
	g_projectileShader->getRenderState()->enableBlend(SB_SRC_ALPHA, DB_ONE);
	g_projectileShader->getRenderState()->disableDepthWrite();
	g_projectileShader->setScalar("uBirthSize", 300.0f, UNIFORM_FLOAT32);
	g_projectileShader->setScalar("uDeathSize", 10.0f, UNIFORM_FLOAT32);
	g_projectileShader->setVector("uBirthColor", &vec3f(0.5f, 0.5f, 10.2f), 3, UNIFORM_FLOAT32);
	g_projectileShader->setVector("uDeathColor", &vec3f(0.0f, 0.0f, 5.1f), 3, UNIFORM_FLOAT32);
	g_projectileShader->setScalar("uBirthOpacity", 0.95f, UNIFORM_FLOAT32);
	g_projectileShader->setScalar("uDeathOpacity", 0.0f, UNIFORM_FLOAT32);
	g_projectileShader->setScalar("uMaxAge", 3.0f, UNIFORM_FLOAT32);
	g_projectileShader->setTexture("uInputTex", g_starParticleTex);

	g_particleShader->setScalar("fcoef", fcoef, UNIFORM_FLOAT32);
	g_particleShader->getRenderState()->enableBlend(SB_SRC_ALPHA, DB_ONE);
	g_particleShader->getRenderState()->disableDepthWrite();

	g_testShader->setScalar("fcoef", fcoef, UNIFORM_FLOAT32);
	g_testShader->getRenderState()->disableCulling();

	g_explosionShader->setScalar("fcoef", fcoef, UNIFORM_FLOAT32);
	g_explosionShader->setTexture("displacementTex", g_explosionDispl);
	g_explosionShader->setTexture("colorRampTex", g_explosionRamp);
	g_explosionShader->getRenderState()->disableCulling();

	g_UIQuadShader->getRenderState()->disableDepthTest();
	g_UIQuadShader->getRenderState()->disableDepthWrite();

	g_postprocessShader->getRenderState()->disableDepthTest();
	g_postprocessShader->getRenderState()->disableDepthWrite();
	g_postprocessShader->getRenderState()->disableBlend();

	g_lumaShader->getRenderState()->disableDepthTest();
	g_lumaShader->getRenderState()->disableDepthWrite();
	g_lumaShader->getRenderState()->disableBlend();

	g_lumaAdaptShader->getRenderState()->disableDepthTest();
	g_lumaAdaptShader->getRenderState()->disableDepthWrite();
	g_lumaAdaptShader->getRenderState()->disableBlend();

	g_lumaMipMapShader->getRenderState()->disableDepthTest();
	g_lumaMipMapShader->getRenderState()->disableDepthWrite();
	g_lumaMipMapShader->getRenderState()->disableBlend();

	g_gaussBlurShader->getRenderState()->disableDepthTest();
	g_gaussBlurShader->getRenderState()->disableDepthWrite();
	g_gaussBlurShader->getRenderState()->disableBlend();

	g_scaleBiasShader->getRenderState()->disableDepthTest();
	g_scaleBiasShader->getRenderState()->disableDepthWrite();
	g_scaleBiasShader->getRenderState()->disableBlend();
}

void loadTextures(){
	Console::log("Loading Textures...");
	g_skyboxCubemap = SceneGraph::createCubeMap("skyboxCubemap", "spacewars/tex/skybox.jpg", "spacewars/tex/skybox.jpg", "spacewars/tex/skybox.jpg", "spacewars/tex/skybox.jpg", "spacewars/tex/skybox.jpg", "spacewars/tex/skybox.jpg");
	g_earthDiffuseTex = SceneGraph::createTexture("earthDiffuseTex", "spacewars/tex/earth_diffuse.jpg", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
	g_earthNormalTex = SceneGraph::createTexture("earthNormalTex", "spacewars/tex/earth_normals.png", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
	g_earthSpecularTex = SceneGraph::createTexture("earthSpecularTex", "spacewars/tex/earth_specular.jpg", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);

	g_spaceship01DiffuseTex    = SceneGraph::createTexture("spaceship01DiffuseTex", "spacewars/models/spaceship01/spaceship01_diffuse.jpg", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
	g_spacestationDiffuseTex   = SceneGraph::createTexture("spacestationDiffuseTex", "spacewars/models/spacestation/spacestation.jpg", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
	g_meteorDiffuseTex         = SceneGraph::createTexture("meteor01DiffuseTex", "spacewars/models/meteor01/meteor01_diff.png", true, TEXTURE_FILTER_BILINEAR, TEXTURE_WRAP_REPEAT);
	g_pointParticleTex         = SceneGraph::createTexture("pointParticleTex", "spacewars/tex/point_particle.png", true, TEXTURE_FILTER_NEAREST, TEXTURE_WRAP_REPEAT);
	g_starParticleTex          = SceneGraph::createTexture("starParticleTex", "spacewars/tex/star_particle.png", true, TEXTURE_FILTER_NEAREST, TEXTURE_WRAP_REPEAT);
	g_HUDTex                   = SceneGraph::createTexture("HUDTex", "spacewars/tex/HUD.png", true, TEXTURE_FILTER_BILINEAR, TEXTURE_WRAP_CLAMP);
	g_HUDSpinnerTex            = SceneGraph::createTexture("HUDSpinnerTex", "spacewars/tex/HUDSpinner.png", true, TEXTURE_FILTER_BILINEAR, TEXTURE_WRAP_CLAMP);
	g_explosionDispl           = SceneGraph::createTexture("explosionDisplacement", "/spacewars/tex/explosion_displ.png", false, TEXTURE_FILTER_BILINEAR, TEXTURE_WRAP_MIRRORED_REPEAT);
	g_explosionRamp            = SceneGraph::createTexture("explosionColorRamp", "spacewars/tex/explosion_ramp.png", false, TEXTURE_FILTER_BILINEAR, TEXTURE_WRAP_CLAMP);
}

void initWorlds(){
	g_mainWorld = SceneGraph::createWorld("mainWorld");
	sun = SceneGraph::createLight("sunLight");
	sun->setTranslate(-1.0f, 0.0f, 0.0f);

	g_particleWorld = SceneGraph::createWorld("particleWorld");
	g_UIWorld = SceneGraph::createWorld("UIWorld");
}

void initCameras(){
	g_playerCam = SceneGraph::createCamera("playerCam");
	g_playerCam->setPerspectiveProjection(1.22f, aspect, nearplane, k_farplane);

	g_UICam = SceneGraph::createCamera("UI_cam");
	g_UICam->setOrthogonalProjection(0.0f, 1.0f, 0.0f, 1.0f / aspect, 0.0f, 10.0f);
	g_UICam->setTranslate(0.0f, 0.0f, 10.0f);

	g_mainWorld->setActiveCamera(g_playerCam);
	g_particleWorld->setActiveCamera(g_playerCam);
	g_UIWorld->setActiveCamera(g_UICam);
}

void initGeometry(){
	Console::log("Creating Geometry...");

	//Earth
	VertexArray *va = createSphere(200, 200, k_earthRadius);
	g_earthGeom     = SceneGraph::createGeometry("earthGeometry", va);
	g_earthGeom->setTexture("DiffuseTex", g_earthDiffuseTex);
	g_earthGeom->setTexture("NormalTex", g_earthNormalTex);
	g_earthGeom->setTexture("SpecularTex", g_earthSpecularTex);
	g_earthGeom->setShaderProgram(g_earthShader);
	g_mainWorld->attachChild(g_earthGeom);

	//Atmosphere
	va               = createSphere(200, 200, k_atmosphereRadius);
	g_atmosphereGeom = SceneGraph::createGeometry("atmosphereGeometry", va);
	g_atmosphereGeom->setShaderProgram(g_atmosphereShader);
	g_atmosphereGeom->setVisible(false);
	g_earthGeom->attachChild(g_atmosphereGeom);

	//Skybox
	va           = createSphere(10, 10, k_farplane / 2.1f);
	g_skyboxGeom = SceneGraph::createGeometry("skyboxGeometry", va);
	g_skyboxGeom->setShaderProgram(g_skyboxShader);
	g_skyboxGeom->setCubeMap("SkyboxTex", g_skyboxCubemap);
	g_skyboxGeom->setVisible(false);


	//Spaceship 1
	g_spaceship01Geom = SceneGraph::createGeometry("spaceship01Geom", "/spacewars/models/spaceship01/spaceship01.obj");
	g_spaceship01Geom->setShaderProgram(g_phongTextureShader);
	g_spaceship01Geom->setTexture("DiffuseTex", g_spaceship01DiffuseTex);
	g_spaceship01Geom->setVector("ka", vec3f(0.05f).vec, 3, UNIFORM_FLOAT32);
	g_spaceship01Geom->setVector("ks", vec3f(0.3f).vec, 3, UNIFORM_FLOAT32);
	g_spaceship01Geom->setScalar("shininess", 0.8f, UNIFORM_FLOAT32);

	//Spacestation
	g_spacestationGeom = SceneGraph::createGeometry("spacestationGeom", "/spacewars/models/spacestation/spacestation.obj");
	g_spacestationGeom->setShaderProgram(g_lambertTextureShader);
	g_spacestationGeom->setTexture("DiffuseTex", g_spacestationDiffuseTex);
	g_spacestationGeom->setVector("ka", vec3f(0.05f).vec, 3, UNIFORM_FLOAT32);

	//Meteors
	g_meteorGeom = SceneGraph::createGeometry("meteor01Geom", "spacewars/models/meteor01/meteor01.obj");
	g_meteorGeom->setShaderProgram(g_lambertTextureShader);
	g_meteorGeom->setTexture("DiffuseTex", g_meteorDiffuseTex);
	g_meteorGeom->setVector("ka", vec3f(0.05f).vec, 3, UNIFORM_FLOAT32);

	//Explosion
	g_explosionGeometry = SceneGraph::createGeometry("explosionGeom", createSphere(20, 20, 5.0));
	g_explosionGeometry->setShaderProgram(g_explosionShader);

	//Sun
	g_sunGeom = SceneGraph::createGeometry("sunGeometry", createSphere(20, 20, 2000));
	g_sunGeom->setShaderProgram(g_testShader);
	

	//HUD & UI
	va = create2DQuad();
	g_HUDSpinnerGeom = SceneGraph::createGeometry("HUDSpinnerGeom", va, true);
	g_HUDSpinnerGeom->setShaderProgram(g_UIQuadShader);
	g_HUDSpinnerGeom->setScale(0.1f, 0.1f, 1.0f);
	g_HUDSpinnerGeom->setTranslate(0.5f, 0.5f / aspect, 0.0f);
	g_HUDSpinnerGeom->setTexture("Tex", g_HUDSpinnerTex);
	g_UIWorld->attachChild(g_HUDSpinnerGeom);

	g_HUDGeom = SceneGraph::createGeometry("HUDGeom", va, true);
	g_HUDGeom->setShaderProgram(g_UIQuadShader);
	g_HUDGeom->setScale(0.1f, 0.1f, 1.0f);
	g_HUDGeom->setTranslate(0.5f, 0.5f / aspect, 0.0f);
	g_HUDGeom->setTexture("Tex", g_HUDTex);
	g_UIWorld->attachChild(g_HUDGeom);

	g_fsQuadGeom = SceneGraph::createGeometry("fsQuadGeom", va);

	ExplosionManager::init();
	ProjectileManager::init();
	MeteorManager::init();
}

void initPlayer(){
	g_playerEngineParticles[0] = new ParticleEmitter("playerEnginePEMain", std::shared_ptr<EmitterSettings_t>(new BoxEmitterSettings(0.0f, 0.1f, 0.0f)), 2000);
	g_playerEngineParticles[1] = new ParticleEmitter("playerEnginePESide1", std::shared_ptr<EmitterSettings_t>(new BoxEmitterSettings(0.0f, 0.03f, 0.0f)), 2000);
	g_playerEngineParticles[2] = new ParticleEmitter("playerEnginePESide2", std::shared_ptr<EmitterSettings_t>(new BoxEmitterSettings(0.0f, 0.03f, 0.0f)), 2000);
	
	g_playerEngineParticles[0]->setVisible(false);
	g_playerEngineParticles[1]->setVisible(false);
	g_playerEngineParticles[2]->setVisible(false);
	particleProxyMain = new Proxy("playerEnginePEProxyMain", g_playerEngineParticles[0]);
	g_particleWorld->attachChild(particleProxyMain);
	particleProxySide1 = new Proxy("playerEnginePEProxySide1", g_playerEngineParticles[1]);
	g_particleWorld->attachChild(particleProxySide1);
	particleProxySide2 = new Proxy("playerEnginePEProxySide2", g_playerEngineParticles[2]);
	g_particleWorld->attachChild(particleProxySide2);

	g_playerEngineParticles[0]->setShaderProgram(g_particleShader);
	g_playerEngineParticles[1]->setShaderProgram(g_particleShader);
	g_playerEngineParticles[2]->setShaderProgram(g_particleShader);

	g_playerEngineParticles[0]->translate(0.0f, 0.042f, 1.0);
	g_playerEngineParticles[0]->setScale(0.08f, 0.15f, 0.08f);
	g_playerEngineParticles[0]->rotateX(fPI / 2.0f);
	g_playerEngineParticles[0]->setScalar("uBirthSize", 200.0f, UNIFORM_FLOAT32);
	g_playerEngineParticles[0]->setScalar("uDeathSize", 50.0f, UNIFORM_FLOAT32);
	g_playerEngineParticles[0]->setVector("uBirthColor", &vec3f(10.0f, 10.0f, 12.0f), 3, UNIFORM_FLOAT32);
	g_playerEngineParticles[0]->setVector("uDeathColor", &vec3f(3.0f, 3.0f, 8.0f), 3, UNIFORM_FLOAT32);
	g_playerEngineParticles[0]->setScalar("uBirthOpacity", 0.9f, UNIFORM_FLOAT32);
	g_playerEngineParticles[0]->setScalar("uDeathOpacity", 0.01f, UNIFORM_FLOAT32);
	g_playerEngineParticles[0]->setScalar("uMaxAge", 0.1f, UNIFORM_FLOAT32);
	g_playerEngineParticles[0]->setTexture("uInputTex", g_pointParticleTex);

	g_playerEngineParticles[1]->translate(0.4f, -0.07f, 0.9f);
	g_playerEngineParticles[1]->setScale(0.08f, 0.1f, 0.08f);
	g_playerEngineParticles[1]->rotateX(fPI / 2.0f);
	g_playerEngineParticles[1]->setScalar("uBirthSize", 150.0f, UNIFORM_FLOAT32);
	g_playerEngineParticles[1]->setScalar("uDeathSize", 5.0f, UNIFORM_FLOAT32);
	g_playerEngineParticles[1]->setVector("uBirthColor", &vec3f(10.0f, 10.0f, 12.0f), 3, UNIFORM_FLOAT32);
	g_playerEngineParticles[1]->setVector("uDeathColor", &vec3f(3.0f, 3.0f, 8.0f), 3, UNIFORM_FLOAT32);
	g_playerEngineParticles[1]->setScalar("uBirthOpacity", 0.8f, UNIFORM_FLOAT32);
	g_playerEngineParticles[1]->setScalar("uDeathOpacity", 0.01f, UNIFORM_FLOAT32);
	g_playerEngineParticles[1]->setScalar("uMaxAge", 0.03f, UNIFORM_FLOAT32);
	g_playerEngineParticles[1]->setTexture("uInputTex", g_pointParticleTex);

	g_playerEngineParticles[2]->translate(-0.4f, -0.07f, 0.9f);
	g_playerEngineParticles[2]->setScale(0.08f, 0.1f, 0.08f);
	g_playerEngineParticles[2]->rotateX(fPI / 2.0f);
	g_playerEngineParticles[2]->setScalar("uBirthSize", 150.0f, UNIFORM_FLOAT32);
	g_playerEngineParticles[2]->setScalar("uDeathSize", 5.0f, UNIFORM_FLOAT32);
	g_playerEngineParticles[2]->setVector("uBirthColor", &vec3f(10.0f, 10.0f, 12.0f), 3, UNIFORM_FLOAT32);
	g_playerEngineParticles[2]->setVector("uDeathColor", &vec3f(3.0f, 3.0f, 8.0f), 3, UNIFORM_FLOAT32);
	g_playerEngineParticles[2]->setScalar("uBirthOpacity", 0.8f, UNIFORM_FLOAT32);
	g_playerEngineParticles[2]->setScalar("uDeathOpacity", 0.01f, UNIFORM_FLOAT32);
	g_playerEngineParticles[2]->setScalar("uMaxAge", 0.03f, UNIFORM_FLOAT32);
	g_playerEngineParticles[2]->setTexture("uInputTex", g_pointParticleTex);

	g_spaceship01Geom->attachChild(g_playerEngineParticles[0]);
	g_spaceship01Geom->attachChild(g_playerEngineParticles[1]);
	g_spaceship01Geom->attachChild(g_playerEngineParticles[2]);

	g_playerGunProjectiles = new ProjectileParticleSystem("playerProjectileParticles", 5.0f);
	g_playerGunProjectiles->setShaderProgram(g_particleShader);
	g_playerGunProjectiles->setScalar("uBirthSize", 50.0f, UNIFORM_FLOAT32);
	g_playerGunProjectiles->setScalar("uDeathSize", 300.0f, UNIFORM_FLOAT32);
	g_playerGunProjectiles->setVector("uBirthColor", &vec3f(4.0f, 4.0f, 6.0f), 3, UNIFORM_FLOAT32);
	g_playerGunProjectiles->setVector("uDeathColor", &vec3f(0.0f, 0.0f, 2.0f), 3, UNIFORM_FLOAT32);
	g_playerGunProjectiles->setScalar("uBirthOpacity", 0.9f, UNIFORM_FLOAT32);
	g_playerGunProjectiles->setScalar("uDeathOpacity", 0.9f, UNIFORM_FLOAT32);
	g_playerGunProjectiles->setScalar("uMaxAge", 5.0f, UNIFORM_FLOAT32);
	g_playerGunProjectiles->setTexture("uInputTex", g_pointParticleTex);
	g_particleWorld->attachChild(g_playerGunProjectiles);

	g_playerGroup = SceneGraph::createGroup("playerGroup");
	g_cameraPivot = SceneGraph::createGroup("cameraPivot");
	g_mainWorld->attachChild(g_playerGroup);
	g_playerGroup->attachChild(g_spaceship01Geom);
	g_playerGroup->attachChild(g_cameraPivot);
	g_cameraPivot->attachChild(g_playerCam);
	g_playerCam->attachChild(g_skyboxGeom);

	g_playerCam->translate(cameraOffset);
	g_playerGroup->translate(g_spacestationLoc-vec3f(100.0f, 10.0f,0.0f));
	g_playerGroup->setRotateY(fPI);
}

void initSpacestation(){
	g_spacestationGeom->translate(g_spacestationLoc);
	g_mainWorld->attachChild(g_spacestationGeom);
	
}

void initGameplayObjects(){
	Console::log("Preparing the magic...");

	initPlayer();
	initSpacestation();

	g_sunGeom->setTranslate(sun->getWorldPosition()*80000);
	g_mainWorld->attachChild(g_sunGeom);
	//g_mainWorld->attachChild(g_meteorGeom);
	//g_meteorGeom->translate(0.0, 0.0, -29970.0f);
}


void RCInit()
{
	Renderer::setClearColor(vec4f(0.0f, 0.0f, 0.0f, 1.0f));

	rt_HDR = SceneGraph::createRenderTarget("rt_HDR", RC_DEFAULT_DISPLAY_WIDTH, RC_DEFAULT_DISPLAY_HEIGHT, 1, true, false, TEXTURE_FILTER_NEAREST, TEXTURE_FORMAT_RGBA16F);
	rt_AuxA = SceneGraph::createRenderTarget("rt_AuxA", auxBuffWidth, auxBuffHeight, 1, false, false, TEXTURE_FILTER_NEAREST, TEXTURE_FORMAT_RGBA16F);
	rt_AuxB = SceneGraph::createRenderTarget("rt_AuxB", auxBuffWidth, auxBuffHeight, 1, false, false, TEXTURE_FILTER_NEAREST, TEXTURE_FORMAT_RGBA16F);
	rt_AuxC = SceneGraph::createRenderTarget("rt_AuxC", auxBuffWidth, auxBuffHeight, 1, false, false, TEXTURE_FILTER_NEAREST, TEXTURE_FORMAT_RGBA16F);

	u32 nMipLevels = (u32)log2f((f32)lumaBuffSize);
	rt_Luma = new RenderTarget*[nMipLevels + 1];
	u32 buffSize = lumaBuffSize;
	for (u32 i = 0; i < nMipLevels + 1; ++i){
		std::string name("rt_Luma");
		name.append(std::to_string(i));
		rt_Luma[i] = SceneGraph::createRenderTarget(name.c_str(), buffSize, buffSize, 1, false, false, TEXTURE_FILTER_NEAREST/*	GL_LINEAR_MIPMAP_NEAREST*/, TEXTURE_FORMAT_RGBA16F);
		buffSize /= 2;
	}

	for (int i = 0; i < 2; ++i) {
		std::string name("rt_AdaptLuma");
		name.append(std::to_string(i));
		rt_AdaptLuma[i] = SceneGraph::createRenderTarget(name.c_str(), 1, 1, 1, false, false, TEXTURE_FILTER_NEAREST, TEXTURE_FORMAT_R16F);
	}

	initWorlds();
	initCameras();
	loadTextures();
	loadShaders();
	

	initGeometry();
	initGameplayObjects();
	setShaderConstants();


	/*enemyGeometry = SceneGraph::createGeometry("enemy_spaceship", createSphere(4, 4, 1));
	enemyGeometry->setShaderProgram(testShader);
	g_mainWorld->attachChild(enemyGeometry);
	enemyCollider = new SphereCollider("enemy_spaceship_collider", std::bind(testHandler, std::placeholders::_1));
	enemyGeometry->attachChild(enemyCollider);
	enemyGeometry->translate(vec3f(0.0f, 0.0f, -19800.0f));
	enemyGeometry->scale(5.0f);
	enemyCollider->scale(5.0f);*/
}

u32 RCUpdate()
{
	f32 dt = Platform::getFrameTimeStep();

	// GET IO STATES
	bool *mouse = Platform::getMouseButtonState();
	vec2f mouse_pos = Platform::getMousePosition();
	bool *keys = Platform::getKeyState();
	bool flying = false;
	// UPDATE KEYBAORD
	if (keys[RC_KEY_SHIFT])
	{
		playerSpeed.x = evalLERP(playerSpeed.x, maxSpeed.x, dt);
		flying = true;
	}
	else{
		playerSpeed.x = evalLERP(playerSpeed.x, 0.0f, 5.0f*dt);
	}

	if (keys[RC_KEY_D]){
		playerSpeed.z = evalLERP(playerSpeed.z, -maxSpeed.z, dt);
		flying = true;
	}
	else if (keys[RC_KEY_A]){
		playerSpeed.z = evalLERP(playerSpeed.z, maxSpeed.z, dt);
		flying = true;
	}
	else{
		playerSpeed.z = evalLERP(playerSpeed.z, 0.0f, 1.5f*dt);
	}

	if (keys[RC_KEY_S]){
		playerSpeed.y = evalLERP(playerSpeed.y, maxSpeed.y, dt);
		flying = true;
	}
	else if (keys[RC_KEY_W]){
		playerSpeed.y = evalLERP(playerSpeed.y, -maxSpeed.y, dt);
		flying = true;
	}
	else{
		playerSpeed.y = evalLERP(playerSpeed.y, 0.0f, 1.5f*dt);
	}

	if (keys[RC_KEY_CTRL]){
		if (!cameraLockToggled){
			cameraLocked = !cameraLocked;
			cameraLockToggled = true;
		}
	}
	else{
		cameraLockToggled = false;
	}

	//UPDATE MOUSE
	if (mouse[RC_MOUSE_LEFT]){
		if (!fired){
			fired = true;
			vec3f pos = g_spaceship01Geom->getWorldPosition();
			vec3f focalpoint = g_playerCam->getWorldPosition() + 100000.0f*g_playerCam->getWorldFront();
			ProjectileManager::spawnProjectile(pos, (focalpoint - pos).normalize()*50.0f);
			//g_playerGunProjectiles->launchProjectile(pos, (focalpoint - pos).normalize()*50.0f);
		}
	}
	else{
		fired = false;
	}
	cameraRotation.x = evalLERP(cameraRotation.x, (cameraLocked) ? 0.0f : (-mouse_pos.x * 2.0f), dt);
	cameraRotation.y = evalLERP(cameraRotation.y, (cameraLocked) ? 0.0f : (mouse_pos.y * 2.0f), dt);
	g_cameraPivot->setRotateX(cameraRotation.y);
	g_cameraPivot->rotateY(cameraRotation.x);

	//UPDATE PLAYER
	g_playerGroup->rotateY(playerSpeed.z*dt);
	g_spaceship01Geom->setRotateZ((playerSpeed.z / maxSpeed.z)*fPI / 4.0f);
	g_spaceship01Geom->rotateX((playerSpeed.y / maxSpeed.y)*fPI / 4.0f);
	g_playerGroup->translate(g_playerGroup->getWorldFront()*playerSpeed.x*dt);
	g_playerGroup->translate(g_playerGroup->getWorldUp()*playerSpeed.y*dt);
	float engineThrottle = 0.0f;
	if (flying)
		engineThrottle = 0.8f + 0.2f*perlin.GetNoise(Platform::getFrameTime());

	g_playerEngineParticles[0]->getSettings()->setPPS((int)(2000 * playerSpeed.x / maxSpeed.x));
	g_playerEngineParticles[1]->getSettings()->setPPS((int)(2000 * playerSpeed.x / maxSpeed.x));
	g_playerEngineParticles[2]->getSettings()->setPPS((int)(2000 * playerSpeed.x / maxSpeed.x));

	g_playerEngineParticles[0]->update(dt);
	g_playerEngineParticles[1]->update(dt);
	g_playerEngineParticles[2]->update(dt);
	
	//g_playerGunProjectiles->update(dt);
	//g_playerGunProjectiles->handleCollisions(enemyCollider, 1);

	g_HUDSpinnerGeom->rotateZ(8.0f*fPI*dt*playerSpeed.x / maxSpeed.x);

	//UPDATE CAMERA
	float cameraHeight = g_mainWorld->getActiveCamera()->getWorldPosition().length();
	g_earthShader->setScalar("fCameraHeight", cameraHeight, UNIFORM_FLOAT32);
	g_earthShader->setScalar("fCameraHeight2", cameraHeight*cameraHeight, UNIFORM_FLOAT32);
	g_atmosphereShader->setScalar("fCameraHeight", cameraHeight, UNIFORM_FLOAT32);
	g_atmosphereShader->setScalar("fCameraHeight2", cameraHeight*cameraHeight, UNIFORM_FLOAT32);

	//Update other objects
	g_earthGeom->rotateY(0.001f*fPI*dt);

	ProjectileManager::updateProjectiles(dt);
	MeteorManager::updateMeteors(dt);
	ExplosionManager::updateExplosions(dt);
	AABox boundingBox;
	g_spacestationGeom->getWorldBoundingBox(&boundingBox);
	
	while (MeteorManager::getAliveCount() < 10)
		MeteorManager::spawnRandMeteor(500.0f);
	u32 hit = MeteorManager::intersects(boundingBox);
	if (hit != -1){
		MeteorManager::destroyMeteor(hit);
		Console::log("We've been hit! Our energy level is at 30%");
	}
	//Renderer::setRenderTarget(RT1);

	//RENDER

	int previousAdaptLuma = currentAdaptLuma;
	currentAdaptLuma = (currentAdaptLuma + 1) % 2;

	Renderer::setRenderTarget(rt_HDR);
	Renderer::clearDepth();
	Renderer::clearColor();
	Renderer::setMatrices(g_mainWorld->getActiveCamera());
	g_skyboxGeom->render();
	//g_sunGeom->render();
	g_mainWorld->renderAll();

	//Render Transparent Objects
	g_atmosphereGeom->render();
	g_particleWorld->renderAll();

	//POSTPROCESS
	//Render Luma
	Renderer::setMatrices(g_UIWorld->getActiveCamera());
	Renderer::setRenderTarget(rt_Luma[0]);
	g_lumaShader->setTexture("uInputTex", rt_HDR->getTexture(0));
	g_fsQuadGeom->setShaderProgram(g_lumaShader);
	g_fsQuadGeom->render();

	//Generate mip maps
	g_fsQuadGeom->setShaderProgram(g_lumaMipMapShader);
	u32 nMipLevels = (u32)log2f((f32)lumaBuffSize);

	for (u32 i = 0; i < nMipLevels; ++i){
		Renderer::setRenderTarget(rt_Luma[i + 1]);
		g_lumaMipMapShader->setTexture("uInputTex", rt_Luma[i]->getTexture(0));
		g_fsQuadGeom->render();
	}

	//adapt luma
	Renderer::setRenderTarget(rt_AdaptLuma[currentAdaptLuma]);
	g_lumaAdaptShader->setScalar("uAdaptionRate", dt* adaptionRate, UNIFORM_FLOAT32);
	g_lumaAdaptShader->setTexture("uLuminanceTex", rt_Luma[nMipLevels]->getTexture(0));
	g_lumaAdaptShader->setTexture("uPrevLuminanceTex", rt_AdaptLuma[previousAdaptLuma]->getTexture(0));
	g_fsQuadGeom->setShaderProgram(g_lumaAdaptShader);
	g_fsQuadGeom->render();

	Renderer::setRenderTarget(rt_AuxA);
	g_scaleBiasShader->setTexture("uInputTex", rt_HDR->getTexture(0));
	g_scaleBiasShader->setVector("uBias", &bloomBias, 4, UNIFORM_FLOAT32);
	g_scaleBiasShader->setVector("uScale", &bloomScale, 4, UNIFORM_FLOAT32);
	g_fsQuadGeom->setShaderProgram(g_scaleBiasShader);
	g_fsQuadGeom->render();

	Renderer::setRenderTarget(rt_AuxB);
	g_gaussBlurShader->setTexture("uInputTex", rt_AuxA->getTexture(0));
	g_gaussBlurShader->setVector("uBlurDirection", &vec2f(1.0f, 0.0f), 2, UNIFORM_FLOAT32);
	g_fsQuadGeom->setShaderProgram(g_gaussBlurShader);
	g_fsQuadGeom->render();

	Renderer::setRenderTarget(rt_AuxC);
	g_gaussBlurShader->setTexture("uInputTex", rt_AuxB->getTexture(0));
	g_gaussBlurShader->setVector("uBlurDirection", &vec2f(0.0f, 1.0f), 2, UNIFORM_FLOAT32);
	g_fsQuadGeom->setShaderProgram(g_gaussBlurShader);
	g_fsQuadGeom->render();

	Renderer::setRenderTarget(rt_HDR);
	g_scaleBiasShader->setVector("uScale", &vec4f(1.0f, 1.0f), 4, UNIFORM_FLOAT32);
	g_scaleBiasShader->setVector("uBias", &vec4f(0.0f, 0.0f), 4, UNIFORM_FLOAT32);
	g_scaleBiasShader->getRenderState()->enableBlend(SB_ONE, DB_ONE);
	g_scaleBiasShader->setTexture("uInputTex", rt_AuxC->getTexture(0));
	g_fsQuadGeom->setShaderProgram(g_scaleBiasShader);
	g_fsQuadGeom->render();
	g_scaleBiasShader->getRenderState()->disableBlend();

	Renderer::setRenderTarget(0);
	Renderer::setMatrices(g_UIWorld->getActiveCamera());
	g_postprocessShader->setTexture("uInputTex", rt_HDR->getTexture(0));
	g_postprocessShader->setTexture("uAdaptedLuminanceTex", rt_AdaptLuma[currentAdaptLuma]->getTexture(0));
	g_fsQuadGeom->setShaderProgram(g_postprocessShader);
	g_fsQuadGeom->render();

	g_UIWorld->renderAll();
	return 0;
}

void RCDestroy()
{
	delete rt_Luma;
	delete particleProxyMain;
	delete particleProxySide1;
	delete particleProxySide2;
	delete g_playerEngineParticles[0];
	delete g_playerEngineParticles[1];
	delete g_playerEngineParticles[2];
	delete g_playerGunProjectiles;
}

#endif