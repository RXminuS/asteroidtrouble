#include "sun.h"

void Sun::init(Node* root, Light* light)
{
	sun_ambient = vec3f(1.0f, 1.0f, 1.0f);	/* Ambient color for the sun (never gets dark!) */
	sun_spin = 0.4f;	/* Angular velocity (spin) of the sun (radians/s) */

	sun = SceneGraph::createGeometry("sun", "scenes/sphere.obj");
	root->attachChild(sun);

	/* Create a shader, set its attributes and attach to sun (uncomment when reaching this point) */
	sun_shader = SceneGraph::createShaderProgram("sun_shader", "LambertTexture", 0);
	sun_shader->setVector("LightPosition", light->getWorldPosition().vec, 3, UNIFORM_FLOAT32);
	sun_shader->setVector("LightColor", light->getColor().vec, 3, UNIFORM_FLOAT32);
	sun_shader->setVector("Ambient", sun_ambient.vec, 3, UNIFORM_FLOAT32);
	sun->setShaderProgram(sun_shader);

	/* Create a texture from file and assign to the sun (uncomment when reaching this point) */
	sun_texture = SceneGraph::createTexture("sun_texture", "textures/sunmap.jpg", true, TEXTURE_FILTER_TRILINEAR, TEXTURE_WRAP_REPEAT);
	sun->setTexture("DiffuseTex", sun_texture);
}

void Sun::update(f32 timeStep)
{
	sun->rotateY( sun_spin * timeStep );
}