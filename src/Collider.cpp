#include "Collider.h"

void SphereCollider::handleCollision(vec3f* point){ 
	Sphere bounds;
	bounds.origin = this->getWorldPosition();
	bounds.setRadius(this->getScale().x);
	if (bounds.encloses(point))
		this->handlerFunc(point);
}