

#include "RenderChimp.h"

/*
	evalLERP

	@param p0
	first control point

	@param p1
	second control point

	@param x
	position between control points (0 < x < 1)
		
	Author(s): Rik
*/

f32 evalLERP(const f32 p0, const f32 p1, const f32 x)
{
	return p0*(1-x)+p1*x;
}

vec3f evalLERP(vec3f &p0, vec3f &p1, const f32 x)
{
	return p0*(1-x)+p1*x;
}

vec2f evalLERP(vec2f &p0, vec2f &p1, const f32 x)
{
	return p0*(1-x)+p1*x;
}

/*
	Catmull-Rom spline interpolation

	Author(s):
*/
vec3f evalCatmullRom(vec3f &p0, vec3f &p1, vec3f &p2, vec3f &p3, const f32 t, const f32 x)
{
	f32 x2 = x*x;
	f32 x3 = x2*x;

	return p1 + (-t*p0 + t*p2)*x + (2*t*p0+(t-3)*p1+(3-2*t)*p2-t*p3)*x2 + (-t*p0+(2-t)*p1+(t-2)*p2+t*p3)*x3;
}

